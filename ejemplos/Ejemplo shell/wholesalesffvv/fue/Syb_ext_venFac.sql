use factor
go
select distinct
'>>|'+convert(varchar(15),A.cli_idc)+';'+convert(varchar(15),C.opo_num)+';FACTORING;'+convert(varchar(15),C.opo_fev,112)+';'+
convert(varchar(15),B.doc_sdo_cli)+';'+A.cli_rso+' '+A.cli_ape_ptn+' '+A.cli_ape_mtn+';'+convert(varchar(15),(C.opo_tas_bas+C.opo_spr_ead))
+';'+convert(varchar(15),B.doc_num)
from doc B, cli A, opo C
where B.doc_fev >= getdate()
and A.cli_idc=B.cli_idc
and B.ope_num=C.opo_num
go