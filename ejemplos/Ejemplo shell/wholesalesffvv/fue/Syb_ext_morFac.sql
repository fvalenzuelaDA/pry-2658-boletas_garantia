use factor
go
select distinct
'>>|'+A.cli_idc+';'+convert(varchar(15),B.doc_num)+';'+convert(varchar(15),B.doc_sdo_cli)+';'+
convert(varchar(8),B.doc_fev,112)+';FACTORING;'+A.cli_rso+' '+A.cli_ape_ptn+' '+A.cli_ape_mtn+';'+convert(varchar(15),B.ope_num)
+';'+convert(varchar(30),C.opo_mto_doc)
from opo C ,doc B, cli A 
where B.pnu_est=2
and C.opo_num= B.ope_num
and C.cli_idc=A.cli_idc
GO
