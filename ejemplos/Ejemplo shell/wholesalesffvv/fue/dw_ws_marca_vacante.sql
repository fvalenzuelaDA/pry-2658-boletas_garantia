

SELECT DATE, TIME;

/* ***************************************************************************************** */
/* CREACION			:	24 DE DICIEMBRE DEL 2018. SERVICIOS Y ASESORIAS ADA LTDA.			 */
/* PROCESO			:	dw_ws_marca_vacante.sql												 */
/* DESCRIPCION		:	MARCA PUESTO QUE ESTAN VACANTES 									 */
/* TABLA DE ENTRADA	:	##BD_TMP##.WS_CTRL_DT_FL0											 */
/* 						##BD_TMP##.WS_CTRL_DT_FL0_POSIC										 */
/* TABLA DE SALIDA	:	##BD_TMP##.WS_COLAB_B												 */
/* EMPRESA			:	ADA LTDA.															 */
/* ***************************************************************************************** */
/* MODIFICACION		:																		 */
/* REALIZADO POR	:																		 */
/* EMPRESA		:																			 */
/* ***************************************************************************************** */

--CREAMOS TABLA CON CAMPO QUE MARCARA LA VACANTE
DROP TABLE ##BD_TMP##.WS_COLAB_B;
CREATE TABLE ##BD_TMP##.WS_COLAB_B
(
	 RUT_EJE			VARCHAR(10)
	,ID_JEF			VARCHAR(10)
	,NOMBRE				VARCHAR(75)
	,APELLIDO_P			VARCHAR(75)
	,APELLIDO_M			VARCHAR(75)
	,CCOST				VARCHAR(30)
	,COD_CARGO			VARCHAR(10)
	,CARGO				VARCHAR(100)
	,CARGO_REAL			VARCHAR(100)
	,ID_EJE				VARCHAR(100)
	,IND_VACANTE		VARCHAR(100)
)UNIQUE PRIMARY INDEX(RUT_EJE);

.IF ERRORCODE <> 0 THEN .QUIT 1;

INSERT INTO ##BD_TMP##.WS_COLAB_B
SELECT
 A.RUT_EJE	
,B.COD_CARGO_JEF	
,A.NOMBRE		
,A.APELLIDO_P
,A.APELLIDO_M
,A.CCOST		
,A.COD_CARGO	
,A.CARGO		
,A.CARGO_REAL
,A.ID_EJE
,'VACANTE'
FROM ##BD_TMP##.WS_CTRL_DT_FL0 A
JOIN ##BD_TMP##.WS_CTRL_DT_FL0_POSIC B
ON A.ID_EJE=B.COD_CARGO
WHERE A.RUT_JEF = 'VACANTE';

.IF ERRORCODE <> 0 THEN .QUIT 2;

SELECT DATE, TIME;