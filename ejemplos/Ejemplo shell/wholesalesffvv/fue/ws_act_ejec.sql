use WholesaleFFVV
GO
select max(dec_fec) as fec into #tmp_fec from dec_detalle_cliente
GO
select dec_ejecutivo into #eje_com from dec_detalle_cliente, #tmp_fec where dec_fec=fec group by dec_ejecutivo
GO
update cde_ctr_dot_eje
set A.cde_ind_eje=0
GO
update cde_ctr_dot_eje
set A.cde_ind_eje=1
from cde_ctr_dot_eje A, #eje_com B
where A.cde_nt=B.dec_ejecutivo
GO
drop table #tmp_fec
GO
drop table #eje_com
GO
