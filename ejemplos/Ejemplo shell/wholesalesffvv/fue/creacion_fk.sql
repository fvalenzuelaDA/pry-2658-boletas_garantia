use WholesaleFFVV
go
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_id('toc_total_clientes') AND constrid=OBJECT_ID('fk_toc_1'))
BEGIN
	ALTER TABLE toc_total_clientes DROP CONSTRAINT fk_toc_1
END
go
ALTER TABLE toc_total_clientes
  ADD CONSTRAINT fk_toc_1
  FOREIGN KEY(dec_rut,dec_fec) REFERENCES dec_detalle_cliente (dec_rut,dec_fec)
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_id('ven_vencimiento') AND constrid=OBJECT_ID('fk_ven_1'))
BEGIN
	ALTER TABLE ven_vencimiento DROP CONSTRAINT fk_ven_1
END
go
ALTER TABLE ven_vencimiento
  ADD CONSTRAINT fk_ven_1
  FOREIGN KEY(dec_rut) REFERENCES toc_total_clientes (dec_rut)
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_id('mor_mora') AND constrid=OBJECT_ID('fk_mor_1'))
BEGIN
	ALTER TABLE mor_mora DROP CONSTRAINT fk_mor_1
END
go
ALTER TABLE mor_mora
  ADD CONSTRAINT fk_mor_1
  FOREIGN KEY(dec_rut) REFERENCES toc_total_clientes (dec_rut)
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_id('rco_relacion_cliente_oficina') AND constrid=OBJECT_ID('fk_rco_1'))
BEGIN
	ALTER TABLE rco_relacion_cliente_oficina DROP CONSTRAINT fk_rco_1
END
go
ALTER TABLE rco_relacion_cliente_oficina
  ADD CONSTRAINT fk_rco_1
  FOREIGN KEY(dec_rut) REFERENCES toc_total_clientes (dec_rut)
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_id('rco_relacion_cliente_oficina') AND constrid=OBJECT_ID('fk_rco_2'))
BEGIN
	ALTER TABLE rco_relacion_cliente_oficina DROP CONSTRAINT fk_rco_2
END
go
ALTER TABLE rco_relacion_cliente_oficina
  ADD CONSTRAINT fk_rco_2
  FOREIGN KEY(dof_cod) REFERENCES dof_detalle_oficina (dof_cod)
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_id('inc_indicador_cliente') AND constrid=OBJECT_ID('fk_inc_1'))
BEGIN
	ALTER TABLE inc_indicador_cliente DROP CONSTRAINT fk_inc_1
END
go
ALTER TABLE inc_indicador_cliente
  ADD CONSTRAINT fk_inc_1
  FOREIGN KEY(dec_rut) REFERENCES inc_indicador_cliente (dec_rut)
GO
