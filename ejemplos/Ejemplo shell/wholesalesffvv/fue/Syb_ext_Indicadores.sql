Select '>>|'+convert(varchar(40),A.rut_cli)+';'+convert(varchar(40),B.pi)+';'+
		convert(varchar(40),D.Capital_Total)+';'+convert(varchar(40),D.RAR_Periodo)+'|' as indicadores
from  
	qwrtb.Metarating A
left outer join 
	qwrtb.Mant_Metarating B
	on A.rating_pricing_num = b.num_rating
left outer join 
	temporal.arb_clieje_datsum_tst	C
	on A.rut_cli = CAST(c.rut_cli AS INTEGER)
left outer join 
	qwrtb.TBL_RGS_PONDERADOR_RC2_arb		D
	on A.rut_cli = D.Rut
where 
	trim(C.bca) in ('C1','C1A','C1B','C1C','C1D','C2','EG','EGQ','EM','EMQ','EX','I1','I2','I3','I4','I5','I6')
	AND A.ano_mes=CONVERT(INTEGER,CONVERT(CHAR(6),GETDATE(),112))
go
