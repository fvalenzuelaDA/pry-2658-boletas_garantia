use WholesaleFFVV
go
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_ID('toc_total_clientes') AND constrid=OBJECT_ID('fk_toc_1'))
BEGIN
	ALTER TABLE toc_total_clientes DROP CONSTRAINT fk_toc_1
END
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_ID('ven_vencimiento') AND constrid=OBJECT_ID('fk_ven_1'))
BEGIN
	ALTER TABLE ven_vencimiento DROP CONSTRAINT fk_ven_1
END
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_ID('mor_mora') AND constrid=OBJECT_ID('fk_mor_1'))
BEGIN
	ALTER TABLE mor_mora DROP CONSTRAINT fk_mor_1
END
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_ID('rco_relacion_cliente_oficina') AND constrid=OBJECT_ID('fk_rco_1'))
BEGIN
	ALTER TABLE rco_relacion_cliente_oficina DROP CONSTRAINT fk_rco_1
END
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_ID('rco_relacion_cliente_oficina') AND constrid=OBJECT_ID('fk_rco_2'))
BEGIN
	ALTER TABLE rco_relacion_cliente_oficina DROP CONSTRAINT fk_rco_2
END
GO
IF EXISTS (SELECT * FROM sysconstraints WHERE tableid=OBJECT_id('inc_indicador_cliente') AND constrid=OBJECT_ID('fk_inc_1'))
BEGIN
	ALTER TABLE inc_indicador_cliente DROP CONSTRAINT fk_inc_1
END
go
