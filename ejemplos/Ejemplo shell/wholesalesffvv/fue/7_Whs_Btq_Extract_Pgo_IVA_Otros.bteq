

SELECT DATE, TIME;
DIAGNOSTIC "COLLECTSTATS, SAMPLESIZE=10" ON FOR SESSION;
/* **************************************************************************************************** */
/* ********************************************* WHOLESALES ******************************************* */
/* **************************************************************************************************** */
/* CREACION				:	10 AGOSTO DEL 2017, GABRIEL DIAZ											*/
/* MODULO				:	EXTRACTOR CON INDICADOR DE PAGOS IVA Y OTROS					 			*/
/* DESCRIPCION			:	EXTRACTOR DE LA INFORMACIÓN DE PAGOS IVA Y OTROS DE LOS CLIENTES DEL BCO	*/
/* TABLA DE ENTRADA		:	${BD_TEMP}.WHS_UNIV_INI_CLI													*/
/* TABLA DE SALIDA		:	${BD_TEMP}.WHS_EXT_PGO_IVA_OTROS											*/
/* EMPRESA				:	ADA LTDA.																	*/
/* **************************************************************************************************** */
/* OBJ.MANTENCION: CAMBIO DATAMART PYME POR DATAMART EMPRESA 										    */
/* AUTOR: NICOLAS SALAZAR 																			    */
/* FECHA: 13 NOVIEMBRE 2018 																		    */
/* **************************************************************************************************** */


/* **************************************************************************************************** */
/* SE OBTIENEN LOS REGISTROS DE PAGO IVA Y OTROS DESDE DATAMART PYME DENTRO DE LOS ULTIMOS 6 MESES		*/
/* **************************************************************************************************** */
DROP TABLE ${BD_TEMP}.WHS_EVT_IVA_OTROS_6M;
CREATE TABLE ${BD_TEMP}.WHS_EVT_IVA_OTROS_6M
(
	  PARTY_ID				INTEGER
	 ,TENENCIA_PGO_IVA		INTEGER
	 ,TENENCIA_PGO_OTROS	INTEGER
	 ,CANT_TENENCIA			INTEGER
)
PRIMARY INDEX (PARTY_ID);
.IF ERRORCODE <> 0 THEN .QUIT 1;

INSERT INTO ${BD_TEMP}.WHS_EVT_IVA_OTROS_6M
	SELECT
		 A.PARTY_ID
		,CASE WHEN A.TIPO_CONVENIO = 'IVA' THEN 1 ELSE 0 END AS TENENCIA_PGO_IVA
		,CASE WHEN A.TIPO_CONVENIO <> 'IVA' THEN 1 ELSE 0 END AS TENENCIA_PGO_OTROS
		,COUNT(1) AS CANT_TENENCIA
	FROM ${BD_DMEMPRESA}.DME_PAGO_IMPUESTO_CNV A
		,${BD_TEMP}.WHS_FECHA_PROCESO B
	WHERE A.FEC_PAGO BETWEEN B.ULT6MESES AND B.PERIODO_FIN
	GROUP BY 1,2,3;
.IF ERRORCODE <> 0 THEN .QUIT 2;

COLLECT STATISTICS USING SAMPLE ${BD_TEMP}.WHS_EVT_IVA_OTROS_6M COLUMN PARTY_ID;


/* **************************************************************************************************** */
/* SE OBTIENE EL REGISTRO DE PAGO IVA Y OTROS A NIVEL DE CLIENTE										*/
/* **************************************************************************************************** */
DROP TABLE ${BD_TEMP}.WHS_EXT_IVA_OTROS_CLI;
CREATE TABLE ${BD_TEMP}.WHS_EXT_IVA_OTROS_CLI
(
	 PARTY_ID					INTEGER
	,TENENCIA_PGO_IVA_IND		INTEGER
	,TENENCIA_PGO_OTROS_IND		INTEGER
)
UNIQUE PRIMARY INDEX (PARTY_ID);
.IF ERRORCODE <> 0 THEN .QUIT 3;

INSERT INTO ${BD_TEMP}.WHS_EXT_IVA_OTROS_CLI
	SELECT
		 PARTY_ID
		,SUM(TENENCIA_PGO_IVA) AS TENENCIA_PGO_IVA_IND
		,SUM(TENENCIA_PGO_OTROS) AS TENENCIA_PGO_OTROS_IND
	FROM ${BD_TEMP}.WHS_EVT_IVA_OTROS_6M
	GROUP BY 1;
.IF ERRORCODE <> 0 THEN .QUIT 4;

COLLECT STATISTICS USING SAMPLE ${BD_TEMP}.WHS_EXT_IVA_OTROS_CLI COLUMN PARTY_ID;


/* **************************************************************************************************** */
/* UNION DEL UNIVERSO CON LAS OPERACIONES OBTENIDAS, AGREGA INDICADOR DE PNOL							*/
/* **************************************************************************************************** */
DROP TABLE ${BD_TEMP}.WHS_EXT_PGO_IVA_OTROS;
CREATE TABLE ${BD_TEMP}.WHS_EXT_PGO_IVA_OTROS
(
	 RUT							INTEGER
	,DV								CHAR(1)
	,CRU_PAGO_IVA_IND				INTEGER
	,CRU_PAGO_ADUANA_Y_OTROS_IND	INTEGER
)
UNIQUE PRIMARY INDEX (RUT);
.IF ERRORCODE <> 0 THEN .QUIT 5;

INSERT INTO ${BD_TEMP}.WHS_EXT_PGO_IVA_OTROS
	SELECT
		 A.RUT
		,A.DV
		,CASE WHEN ZEROIFNULL(B.TENENCIA_PGO_IVA_IND) > 0 THEN 1 ELSE 0 END CRU_PAGO_IVA_IND
		,CASE WHEN ZEROIFNULL(B.TENENCIA_PGO_OTROS_IND) > 0 THEN 1 ELSE 0 END CRU_PAGO_ADUANA_Y_OTROS_IND
	FROM ${BD_TEMP}.WHS_UNIV_INI_CLI A
	LEFT JOIN ${BD_TEMP}.WHS_EXT_IVA_OTROS_CLI B
		ON A.PARTY_ID = B.PARTY_ID;
.IF ERRORCODE <> 0 THEN .QUIT 6;

COLLECT STATISTICS USING SAMPLE ${BD_TEMP}.WHS_EXT_PGO_IVA_OTROS COLUMN RUT;
COLLECT STATISTICS USING SAMPLE ${BD_TEMP}.WHS_EXT_PGO_IVA_OTROS COLUMN DV;


DROP TABLE ${BD_TEMP}.WHS_EVT_IVA_OTROS_6M;
DROP TABLE ${BD_TEMP}.WHS_EXT_IVA_OTROS_CLI;

SELECT DATE, TIME;
.LOGOFF;
.QUIT 0;