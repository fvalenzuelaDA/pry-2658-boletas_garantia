use WholesaleFFVV
go
truncate table toc_total_clientes
GO
insert into toc_total_clientes
select dec_rut, max(dec_fec) from dec_detalle_cliente group by dec_rut
GO
SELECT B.dec_rut into #tmp1
FROM ven_vencimiento B
LEFT JOIN toc_total_clientes A
ON A.dec_rut=B.dec_rut
WHERE A.dec_rut IS null
GO
delete ven_vencimiento from ven_vencimiento A, #tmp1 B
where A.dec_rut=B.dec_rut
GO
drop table #tmp1
GO
SELECT B.dec_rut into #tmp1
FROM mor_mora B
LEFT JOIN toc_total_clientes A
ON A.dec_rut=B.dec_rut
WHERE A.dec_rut IS null
GO
DELETE mor_mora from mor_mora A, #tmp1 B
WHERE A.dec_rut=B.dec_rut
GO
DROP TABLE #tmp1
GO
SELECT A.dof_cod into #tmp1
FROM rco_relacion_cliente_oficina A
LEFT JOIN dof_detalle_oficina B
ON A.dof_cod=B.dof_cod
WHERE B.dof_cod IS NULL
GO
DELETE rco_relacion_cliente_oficina from rco_relacion_cliente_oficina A, #tmp1 B
WHERE A.dof_cod=B.dof_cod
GO
DROP TABLE #tmp1
GO
--se agrega regla de integridad para tabla de indicadores
SELECT A.dec_rut into #tmp1
FROM inc_indicador_cliente A
LEFT JOIN toc_total_clientes B
ON A.dec_rut=B.dec_rut
WHERE B.dec_rut IS NULL
GO
DELETE inc_indicador_cliente from inc_indicador_cliente A, #tmp1 B
WHERE A.dec_rut=B.dec_rut
GO
DROP TABLE #tmp1
GO