#!/usr/bin/sh

#************************************************************************
# Nombre: sh_ws_ext_syb_F.sh
# Ruta: $HOME/she
# Autor: Miguel Pedreros (ADA LTDA) - Ing. SW BCI:
# Fecha: 28/02/2018 
# Descripción:  Obtiene archivos planos con informacion por tramos de Edad, Renta	
#               y rubros
# Parámetros de Entrada: [-e ARCHIVO_SALIDA] [-s] [-p] ARCHIVO_SQL 
#	-e: Genera limpieza de archivo en caso de ser extraccion
#	-p: Printlog, cambia la configuracion para desplegar informacion por pantalla
#	-s:Sobreescritura de Log, cambia la configuracion para sobreescribir o no el archivo Log
# Ejemplo de ejecución: 
#			sh sh_ws_ext_syb_F.sh script.sql
#			sh sh_ws_ext_syb_F.sh -e extraccion.txt script.sql
#			sh sh_ws_ext_syb_F.sh -p script.sql
#			sh sh_ws_ext_syb_F.sh -s script.sql
#			sh sh_ws_ext_syb_F.sh -e extraccion.txt -p script.sql
#			sh sh_ws_ext_syb_F.sh -e extraccion.txt -p -s script.sql	
#************************************************************************

#--------------------------------------------------------------------------------
#	Carga Informacion de Ejecucion
#--------------------------------------------------------------------------------
	INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
	BSE_PGM=`echo ${INF_PGM} |cut -d"." -f1`
	INF_PID=$$
	INF_ARG=$@
	INF_CANT_ARG=$#
	INF_INI=`date +%d/%m/%y" "%H:%M:%S`
	FECHA_PROCESO=$(date +%Y%m%d)
	INF_PRINTLOG="NO"
	INF_SOBRELOG="NO"

# LOG PREVIO A LA CARGA DEL ARCHIVO PROFILE
archLog=$HOME/log/${BSE_PGM}_${INF_PID}.log
#--------------------------------------------------------------------------------
#	DEFINICION DE FUNCIONES BASICAS
#--------------------------------------------------------------------------------
printlogfinal()
{	
	printlog 2 "valida si log existe"
	printlog 2 "x${archLogFnl} = x"
	if [ "x${archLogFnl}" = "x" ]
	then
		#Si el nombre del log real no se ha construido debido a que no se obtuvo el parametro de
		#fecha para la extension
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			 "correctos."
		echo "Favor de ver log de paso ${archLog}"
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			"correctos." >> ${archLog}
		echo "Favor de ver log de paso ${archLog}" >> ${archLog}
		return
	fi

	printlog 2 "valida si log tiene datos"	
	printlog 2 "-s ${archLogFnl}"
	if [ -s ${archLogFnl} ]
	then
		# Si existe el log final, quiere decir que ya existe una ejecucion del proceso para
		# la misma fecha, por lo cual, se debe concatenar el resultado de esta ejecucion
		# con el resultado de la ejecucion anterior en el mismo archivo.
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\""\
			"\"%H:%M:%S` #####" 
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "######### --->                  INICIO EJECUCION `date +%d/%m/%y`                  "\
			"<--- #############" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\""\
			"\"%H:%M:%S` #####" >> ${archLogFnl}
		echo "" ${archLogFnl}
		cat ${archLog} >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "##### Fin de la ejecucion  `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"\
			>> ${archLogFnl}
		rm -f ${archLog}
	else
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\""\
			"\"%H:%M:%S` #####"
		echo "" > ${archLogFnl}
		
		# Si el archivo de log final no existe pero se puedo crear con la linea anterior, se tras-
		#pasa el resultado de la ejecucion del log temporal al log final.
		
		echo "Valida si log final fue creado correctamente" >> ${archLogFnl}		
		printlog 2 "test -s ${archLogFnl}"
		if test -s ${archLogFnl}
		then
			echo "" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "####### --->                  INICIO EJECUCION `date +%d/%m/%y`                "\
					"<--- ###############" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/"\
				"%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
			echo "" ${archLogFnl}
			cat ${archLog} >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###"
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###" >> ${archLogFnl}
			rm -f ${archLog}
		fi
	fi
}

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog 2 "valida retorno de salida"
	if [ $1 -gt 0 ]
	then
		printlog 2 "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
		unifica_archivos_log
	fi
	printlog 1 "************************************************************************************"
	printlog 1 "**                     RESUMEN                                                    **"
	printlog 1 "************************************************************************************"
	printlog 1 "Servidor      : `uname -n`"
	printlog 1 "Usuario       : `whoami`" 
	printlog 1 "Programa      : ${INF_PGM}"
	printlog 1 "ID proceso    : ${INF_PID}"
	printlog 1 "Hora_Inicio   : ${INF_INI}"
	printlog 1 "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog 1 "Parametros    : ${INF_ARG}"
	printlog 1 "Cant. Parametr: ${INF_CANT_ARG}"
	printlog 1 "Resultado     : ${2} "
	printlog 1 "Archivo Log   : ${archLogFnl}"
	printlog 1 "************************************************************************************"
	echo $2 | tee -a ${archLog}
	printlogfinal
	exit $1
}

printlog()
{
	# Parametro 1: Usar "1" para que la informacion salga por pantalla y quede en el log o "2" para que solo quede en el log
	# Parametro 2: El contenido a desplegar
	Hora=$(date +%H:%M:%S)
	echo "${Hora} valida printlog"   >> ${archLog}
	echo "$1 -eq 1 -o x${INF_PRINTLOG} = xSI" >> ${archLog}
	if [ $1 -eq 1 -o "x${INF_PRINTLOG}" = "xSI" ]
	then
		echo ${Hora} "$2" # Salida a Consola
	fi
	echo ${Hora} "$2"  >> ${archLog}
}
#----------------------------------------------------
#	Funciones previa carga del archivo de funciones.
#----------------------------------------------------

# Funcion que valida la existencia de un archivo y que tenga mas de 0 byte
func_valida_archivo ()
{
	printlog 2 "valida la existencia de un archivo"
	if [ ! -f $1 ]
	then 
		printlog 1 "Funcion func_valida_archivo : ERROR No se encuentra el archivo $1."
		SALIR 2 "FALLIDO - No se encuentra el archivo $1"
	fi
	
	printlog 2 "verifica archivo distinto de vacio"
	#verifica archivo distinto de vacio
	if [ -s $1 ]
	then
		printlog 2 "Funcion func_valida_archivo : Archivo $1 OK"
	else
		printlog 2 "Funcion func_valida_archivo : ERROR - El archivo $1 no posee datos."
		SALIR 2 "FALLIDO - El archivo $1 no posee datos"
	fi
}

# Funcion que valida la ejecucion o carga de un archivo (Profile y funciones)
func_valida_carga_archivo ()
{
	printlog 1 "Funcion func_valida_carga_archivo : Validando carga del $2 de funciones..."
	printlog 2 "$1 -ne 0"
	if [ $1 -ne 0 ]
	then
		printlog 2 "Funcion func_valida_carga_archivo : ERROR Archivo $2 no cargado."
		SALIR 2 "FALLIDO - Archivo $2 no cargado."
	else
		printlog 1 "Funcion func_valida_carga_archivo : Archivo $2 encontrado y cargado."
	fi
}

# Funcion que describe como invocar el proceso
Syntax ()
{
	printlog 1 "Sintaxis:"
	printlog 1 "\t ${INF_PGM} [-e ARCHIVO_SALIDA] [-s] [-p] ARCHIVO_SQL"
	printlog 1 ""
	printlog 1 "Parametro"
	printlog 1 "e\t Genera limpieza de archivo en caso de ser extraccion"
	printlog 1 "p\t Printlog, cambia la configuracion predeterminada para desplegar informacion por"\
		" pantalla"
	printlog 1 "s\t Sobreescritura de Log, cambia la configuracion predeterminada para sobreescribir"\
		" o no el archivo Log"
	printlog 1 "Ejemplo:"
	printlog 1 "${INF_PGM} script.sql"
	printlog 1 "${INF_PGM} -e extraccion.txt script.sql"
	printlog 1 "${INF_PGM} -p script.sql"
	printlog 1 "${INF_PGM} -s script.sql"
	printlog 1 "${INF_PGM} -e extraccion.txt -p script.sql"
	printlog 1 "${INF_PGM} -e extraccion.txt -p -s script.sql"
	
}

#--------------------------------------------------------------------------------
# Definicion de archivo Profile y de Funciones
#--------------------------------------------------------------------------------
ArchivoProfile=$HOME/cfg/ws_profile

# Verifica archivo Profile
func_valida_archivo $ArchivoProfile

. $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile


#verifica existencia de archivo de funciones
func_valida_archivo $ArchivoFunciones
. $ArchivoFunciones
func_valida_carga_archivo $? $ArchivoFunciones

valida_definicion_variables "${VARIABLE_AMBIENTE}"

#--------------------------------------------------------------------------------
# Comienza Shell
#--------------------------------------------------------------------------------
# Asignacion de parametros dinamicos
# s Invierte la configuracion de la sobreescritura del log
# p Invierte la configuracion acerca de la informacion que se despliega por pantalla
LIMPIEZA_LOG=NO
ARCHIVO_SALIDA=""
Cant_Min_Param=1

while getopts ":e::s:p" opt; do
	printlog 2 "Se el parametro a validar es $opt."
	case $opt in
	e)
		Cant_Min_Param=$(($Cant_Min_Param+2))
		printlog 2 "Asignacion de Variable: LIMPIEZA_LOG=${LIMPIEZA_LOG}"
		LIMPIEZA_LOG="SI"
		printlog 2 "Asignacion de Variable: LIMPIEZA_LOG=${LIMPIEZA_LOG}"
		printlog 2 "Asignacion de Variable: ARCHIVO_SALIDA=${ARCHIVO_SALIDA}"
		ARCHIVO_SALIDA=$OPTARG
		printlog 2 "Asignacion de Variable: ARCHIVO_SALIDA=${ARCHIVO_SALIDA}"
		;;
	p)
		Cant_Min_Param=$(($Cant_Min_Param+1))
		printlog 2 "Asignacion de Variable: INF_PRINTLOG=${INF_PRINTLOG}"
		if [ "x$INF_PRINTLOG" = "xSI" ]
		then
			INF_PRINTLOG="NO"
		else
			INF_PRINTLOG="SI"
		fi
		printlog 2 "Asignacion de Variable: INF_PRINTLOG=${INF_PRINTLOG}"
		;;
	s)
		Cant_Min_Param=$(($Cant_Min_Param+1))
		printlog 2 "Asignacion de Variable: INF_SOBRELOG=${INF_SOBRELOG}"
		if [ "x$INF_SOBRELOG" = "xSI" ]
		then
			INF_SOBRELOG="NO"
		else
			INF_SOBRELOG="SI"
		fi
		printlog 2 "Asignacion de Variable: INF_SOBRELOG=${INF_SOBRELOG}"
		;;
	\?)
		printlog 1 "Opcion no valida: -$OPTARG" >&2
		Syntax
		SALIR 2 "FALLIDO - Sintaxis invalida"
		;;
	:)
		printlog 1 "Opcion -$OPTARG requiere un parametro." >&2
		Syntax
		SALIR 2 "FALLIDO - Sintaxis invalida"
		;;
	esac
done

# Asignacion del Archivo de Proceso (script que sera ejecutado)
ArchivoProceso=$(echo "$@" | awk 'BEGIN {FS=" "} ; END{print $NF}' )
printlog 2 "Asignacion de Variable: ArchivoProceso=${ArchivoProceso}"

ArchivoProcesoSExt=$(echo $ArchivoProceso | cut -d'.' -f1)
printlog 2 "Asignacion de Variable: ArchivoProcesoSExt=${ArchivoProcesoSExt}"

# Asignacion del log final
printlog 2 "Asignacion del log final"
if [ "x${archLogFnl}" = "x" ]
then
	archLogFnl=${DirLog}/${BSE_PGM}.${ArchivoProcesoSExt}.${FECHA_PROCESO}.log
	printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
fi

# Valida si debe sobreescribir el log
printlog 2 "Valida si debe sobreescribir el log"
printlog 2 "x$INF_SOBRELOG = xSI"
if [ "x$INF_SOBRELOG" = "xSI" ]
then
	eliminar_archivo ${archLogFnl}
fi


Valida_num_param $INF_CANT_ARG $Cant_Min_Param

valida_archivo $DirFue/$ArchivoProceso

Sb_Realiza_Conexion $UyPuserMarte

Ejecuta_Script ${P_USUARIO} ${P_CLAVE} ${ServerIQ} ${DirFue}/${ArchivoProceso} ${DirFtpOut}/${ARCHIVO_SALIDA}

SALIR 0 "CORRECTO"
