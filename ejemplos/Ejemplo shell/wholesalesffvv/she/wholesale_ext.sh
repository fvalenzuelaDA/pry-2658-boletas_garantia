#!/usr/bin/sh
#************************************************************************
# Nombre				: wholesale_ext.sh
# Ruta					: /she/
# Autor					: Gabriel Diaz (Ada) - Ing. SW BCI: Alvaro Torres Medina
# Fecha					: 11/08/2017
# Descripci�n			: Shell que extrae informacion desde Dwh
# Par�metros de Entrada	: fecha proceso (a�omes: yyyymm)
# Ejemplo de ejecuci�n	: sh wholesale_ext.sh 201707
#************************************************************************
# Mantenci�n #1: 
# Autor: Felipe Pousa C (Ada) - Ing. SW BCI: Julio Duran
# Fecha: 06-05-2019
# Descripci�n: Se incorpora validacion de existencia de nuevos procesos
#************************************************************************


date +'%Y-%m-%d %H:%M:%S'
 
#Seteo de Directorios
NomPrc='Extracci�n Wholesale'
export NomPrc
 
###################################################################
#						 Variables  							  #
###################################################################
SHELL_LOG=`date +%Y%m%d.wholesale_ext.log`
export SHELL_LOG
ARCHLOG=`date +%Y%m%d.wholesale_ext.txt`
export ARCHLOG

date '+  %Y-%m-%d %H:%M:%S' > $HOME/log/$SHELL_LOG
date '+%Y-%m-%d %H:%M:%S' > $HOME/log/$ARCHLOG  

server=`uname -n`
dia=`date +'%Y'/'%m'/'%d'`
fec=`date +'%Y%m%d'`
periodo=`date +'%Y%m'`
Hora=`date +'%H':'%M':'%S'`
AMB=`BciAmbiente`
hora_ini=`date '+%H:%M:%S'`


RutaAmbiente=$HOME/cfg/wholesale_ext_ambiente.txt


###################################################################
#						funcion Printlog  						  #
###################################################################
printlog()
{
	H=`date +'%H':'%M':'%S'`
    echo [${H}] "$@" >> $HOME/log/$SHELL_LOG
}

###################################################################
#						Realiza conexion 						  #
###################################################################
Realiza_Conexion ()
{
#---------------------------------------------------------------------
#  OBTIENE DATOS DE CONEXION A LA BASE DE DATOS
#---------------------------------------------------------------------

printlog " "
printlog "*********************************"
printlog "*   OBTTENE DATOS DE CONEXION   *"
printlog "*       A LA BASE DE DATOS      *"
printlog "*********************************"
echo " "
Mensaje "--> Datos de conexion a BD:"

Mensaje "Obteniendo usuario y password..."
printlog "Obteniendo usuario y password..."
uypcnt=0
for uyprsp in `UyPuser $1`
do
 uypDa[$uypcnt]=$uyprsp
      (( uypcnt=$uypcnt+1))
done
printlog "if [ "${uypDa[0]}" = "ERROR" ]"
if [ "${uypDa[0]}" = "ERROR" ] ; then
   Mensaje "ERROR: No se pudo obtener la clave"  
   printlog "ERROR: No se pudo obtener la clave"
   SALIR 2 "ERROR: No se pudo obtener la clave"
else
Mensaje "...usuario y password ok "
printlog "...usuario y password ok "

SYB_U=${uypDa[0]}
SYB_P=${uypDa[1]}

export $SYB_U
export $SYB_P

fi
}

SALIR()
{
	printlog "*********************************************************"
	printlog "**                 RESUMEN                             **"
	printlog "*********************************************************"
	printlog "Servidor    : `uname -n`"
	printlog "Usuario     : `whoami`"
	printlog "Ambiente    : `BciAmbiente`"
	printlog "Programa    : ${INF_PGM}"
	printlog "ID proceso  : ${INF_PID}"
	printlog "Hora Inicio : ${INF_INI}"
	printlog "Hora Termino: `date +%d/%m/%Y\" \"%H:%M:%S`"
	printlog "Parametros  : ${INF_ARG}"
	printlog "Resultado   : $2"
	printlog "Archivo Log : ${LOGFILE}"
	printlog "*********************************************************"
	exit $1
}

INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@


####################################################################
#						Validacion de archivo ambiente y funciones #
####################################################################

printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaAmbiente ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaAmbiente"
	SALIR 2 "ERROR: No se encuentra el archivo $RutaAmbiente"

fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaAmbiente
then
	echo "Archivo $RutaAmbiente OK"
	printlog "Archivo $RutaAmbiente OK"
	. $RutaAmbiente
else
	echo "Error, Archivo sin datos: $RutaAmbiente"
	SALIR 2 "Error, Archivo sin datos: $RutaAmbiente"

fi

printlog "Valida existencia de archivo: $RutaFunc "
if [ ! -f $RutaFunc ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaFunc"
	SALIR 2 "ERROR: No se encuentra el archivo $RutaFunc"

fi
printlog "Valida contenido de archivo: $RutaFunc"
if test -s $RutaFunc
then
	echo "Archivo $RutaFunc OK"
	printlog "Archivo $RutaFunc OK"
	. $RutaFunc
else
	echo "Error, Archivo sin datos: $RutaFunc"
	SALIR 2 "Error, Archivo sin datos: $RutaFunc"

fi


#####################################################################################
#					VALIDIACION DE PARAMETROS A INGRESAR					        #
#####################################################################################
printlog "Se valida el ingreso de 1 parametro: $#"
if [ $# -ne 1 ]
then
	Mensaje "ERROR: Cantidad de parametros mal ingresada: $#"
	SALIR 2 "ERROR: Cantidad de parametros mal ingresada: $#"

else
    validaPerido $1
	fec_prc=$1
	export fec_prc
	repetir=`expr substr $fec_prc 5 6`
	export repetir
	printlog "Parametro Ok. Cantidad ingresada: $#"
fi


###################################################################################################
#						Validiacion de procesos (valida existencia)						          #
###################################################################################################
printlog "Se procede a validar existencia de archivos en directorio $DirCfg y $DirSql "

Valida_Archivo $DirCfg/wsl_Arch_Proc_Extract.txt


Valida_Archivo $DirSql/2_Whs_Btq_Rut_Univ_Cli.bteq
Valida_Archivo $DirSql/3_Whs_Btq_Extract_Inv_DAP_FFMM.bteq
Valida_Archivo $DirSql/4_Whs_Btq_Extract_Sdo_Ccte.bteq
Valida_Archivo $DirSql/5_Whs_Btq_Extract_Pgo_REM_PRV.bteq
Valida_Archivo $DirSql/6_Whs_Btq_Extract_Pgo_PNOL.bteq
Valida_Archivo $DirSql/7_Whs_Btq_Extract_Pgo_IVA_Otros.bteq
Valida_Archivo $DirSql/8_Whs_Btq_Extract_Col.bteq
Valida_Archivo $DirSql/9_Whs_Btq_Univ_Cruce_Prod.bteq
Valida_Archivo $DirSql/10_Whs_Fexp_Cruce_Prod.fexp
Valida_Archivo $DirSql/11_Whs_Extract_Hist_Lea.bteq
Valida_Archivo $DirSql/12_Whs_Extract_Hist_Fac.bteq
Valida_Archivo $DirSql/13_Whs_Unif_Hist_Lea_Fac.bteq
Valida_Archivo $DirSql/14_Whs_Fexp_Hist_Lea_Fac.fexp
Valida_Archivo $DirSql/15_Whs_Extract_Deuda_Bci_Mto.bteq
Valida_Archivo $DirSql/16_Whs_Fexp_Sow_Evolucion_Sow.fexp
Valida_Archivo $DirSql/17_Whs_Extract_Spread_Col.bteq
Valida_Archivo $DirSql/18_Whs_Fexp_Spread_Col_CP.fexp
Valida_Archivo $DirSql/19_Whs_Fexp_Spread_Col_LP.fexp
Valida_Archivo $DirSql/A_Dw_Bancas.fld
Valida_Archivo $DirSql/B_Dw_Uni_Clientes.bteq
Valida_Archivo $DirSql/C_Dw_eva_val_eco_agregado.bteq
Valida_Archivo $DirSql/D_Dw_poa_posicion_actual.bteq
                       
Valida_Archivo $DirIn/universo_clientes.txt


###################################################################################################
#						COMIENZA EJECUCION				                                          #
###################################################################################################
echo " "
echo "################################################################"
echo "# Proceso $nombreProceso Ejecutado el dia `date +%Y-%m-%d %H:%M:%S`"
echo "# Base de Datos Temporal: ${BD_TMP}"
echo "################################################################"
 
 
date '+%Y-%m-%d %H:%M:%S'
printlog " " 
printlog 'INICIO shell wholesale_ext.sh.sh'
Mensaje 'INICIO shell wholesale_ext.sh.sh'
 
printlog ""
printlog "** Realiza Conexion a Teradata -DWH **"
Mensaje "** Realiza Conexion a Teradata -DWH **"
Mensaje "---------------------------------------"

#CONEXION A TERADATA
#CONSEGUIRSE EL UYP .. POR EL MOMENTO SE EJECUTARA CON ARCHIVO LOGON

Realiza_Conexion $UyPuserTera
#SYB_U=Usinfsbp
#SYB_P=usdes214
#CONEXION AUTOMATICA EN DWH

echo ""
echo ".logon ${Alias_DWH}${SYB_U},${SYB_P};" > $LogonFile


#####################################################################
#Comienza ejecucion de BTQ para generar informes                    #
#####################################################################



#obtengo numero de lineas del archivo de Configuraci�n
N_Proc=`wc -l $DirCfg/wsl_Arch_Proc_Extract.txt | awk '{ printf "%i", $1 }'`

inicio=1
while [ $inicio -le $N_Proc ]; do
	printlog "if [ $inicio -lt 10 ]"
	if [ $inicio -lt "10" ]
	then
		indice=0$inicio
	else
		indice=$inicio
	fi

	NombreProc=`awk '$1 ~ /'$indice'/ {print $2}' $DirCfg/wsl_Arch_Proc_Extract.txt`
	TipoProc=`awk '$1 ~ /'$indice'/ {print $3}' $DirCfg/wsl_Arch_Proc_Extract.txt`
	IndEjecProc=`awk '$1 ~ /'$indice'/ {print $4}' $DirCfg/wsl_Arch_Proc_Extract.txt`


	#Ejecuci�n de Modulos *.sql, Identificando si es (Bteq, Fload, Fexport). Tambien si el Indicador de Ejecuci�n = 'S'
	printlog "[ $TipoProc = FLOAD -a $IndEjecProc = S ]"
	if [ $TipoProc = 'FLOAD' -a $IndEjecProc = 'S' ]
	then
		printlog "Ejecutando Modulo $NombreProc"
		echo ".logon ${Alias_DWH}${SYB_U},${SYB_P};" > $LogonFile
		sh  $DirShe/fload.sh $NombreProc
		estado=$?
		#Validamos Ejecuci�n del Modulo
		printlog "[ $estado -ne 0 ]"
		if [ $estado -ne 0 ]
		then
			SALIR 2 "Error en Modulo $NombreProc"
			Eliminar_Archivo $LogonFile

		fi
		printlog "Ok Ejecucion Correcta Modulo $NombreProc"
	fi

	printlog "[ $TipoProc = BTEQ -a $IndEjecProc = S ]"
	if [ $TipoProc = 'BTEQ' -a $IndEjecProc = 'S' ]
	then
		printlog "Ejecutando Modulo $NombreProc"
		echo ".logon ${Alias_DWH}${SYB_U},${SYB_P};" > $LogonFile
		sh $DirShe/bteq.sh $NombreProc
		estado=$?
		#Validamos Ejecuci�n del Modulo
		printlog "[ $estado -ne 0 ]"
		if [ $estado -ne 0 ]
		then
			SALIR 2 "Error en Modulo $NombreProc"
			Eliminar_Archivo $LogonFile

		fi	
		
		printlog "Ok Ejecucion Correcta Modulo $NombreProc"
	fi

	printlog "[ $TipoProc = FEXP -a $IndEjecProc = S ]"
	if [ $TipoProc = 'FEXP' -a $IndEjecProc = 'S' ]
	then
		printlog "Ejecutando Modulo $NombreProc"
		echo ".logon ${Alias_DWH}${SYB_U},${SYB_P};" > $LogonFile
		sh $DirShe/fexp.sh $NombreProc
		estado=$?
		# Validamos Ejecuci�n del Modulo
		printlog "[ $estado -ne 0 ]"
		if [ $estado -ne 0 ]
		then
			SALIR 2 "Error en Modulo $NombreProc"
			Eliminar_Archivo $LogonFile
			
		fi
		printlog "Ok Ejecucion Correcta Modulo $NombreProc"
	fi


let inicio=inicio+1
done

Mensaje 'FIN shell wholesale_ext.sh'
 
SALIR 0 "Termino Exitoso"
