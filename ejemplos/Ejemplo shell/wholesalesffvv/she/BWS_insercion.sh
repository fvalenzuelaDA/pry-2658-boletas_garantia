#!/usr/bin/sh
#************************************************************************
# Nombre				: BWS_Insercion.sh
# Ruta					: /she/
# Autor					: Sebastian Rozas (Ada) - Ing. SW BCI: Alvaro Torres Medina
# Fecha					: 07/08/2017
# Descripción			: carga tablas y mantiene historia
# Parámetros de Entrada	: <tabla>
# 						  <cantidad_meses>
#						  <AAAAMMDD que se cargara>
#						  <AAAAMMDD de inicia para borrar>
# Ejemplo de ejecución	: sh BWS_Insercion.sh <tabla> <cantidad_meses> <AAAAMMDD que se cargara>
#************************************************************************
# Mantención #1: 
# Autor: Nicolas Salazar (Ada) - Ing. SW BCI: Julio Duran
# Fecha: 08-10-2018
# Descripción: Se incorpora logica para reprocesar periodo
#************************************************************************
# Mantención #2: 
# Autor: Nicolas Salazar (Ada) - Ing. SW BCI: Julio Duran
# Fecha: 23-10-2018
# Descripción: Se incorpora logica para cambio de archivo a cargar en
#              dec_detalle_cliente y sow_evolucion_sow
#************************************************************************
# Mantención #3: 
# Autor: Felipe Pousa C (Ada) - Ing. SW BCI: Julio Duran
# Fecha: 06-05-2019
# Descripción: Se modifica Logica de Bci In para que todos los archivos sean cargado 
#			   de igual manera, con archivo de entrada sin extension.	
#************************************************************************
# Mantención #4: 
# Autor: Felipe Pousa C (Ada) - Ing. SW BCI: Julio Duran
# Fecha: 03-06-2019
# Descripción: Para los archivos hlp_historico_largo_plazo.txt, hcp_historico_corto_plazo.txt, hlf_historico_lea_fac.txt y
#			   cru_cruce.txt se cambia el la ruta donde lo busca el bco ip y se les elimina la extension de txt a vacio.
#************************************************************************


sNOMBRE_SHELL="BWS_Insercion.sh"

sLOG=$HOME/log
sCFG=$HOME/cfg

printlog()
{
	Hora=`date +%H:%M:%S`
	echo ${Hora} "$@" # Salida a Consola
	echo ${Hora} "$@"  >> $sLOG/${SHELL_LOG}
}

SHELL_LOG=`date +'%Y%m%d'.BWS_Insercion.${1}.log`
ARCHLOG=`date +'%Y%m%d'.BWS_Insercion.${1}`
date '+  %Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG
server=`uname -n`
dia=`date +'%Y'/'%m'/'%d'`
Hora=`date +'%H':'%M':'%S'`
AMB=`BciAmbiente`
RutaAmbiente="$HOME/cfg/BWS_ambiente.txt"
INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog "if [ $1 -gt 0 ]"
	if [ $1 -gt 0 ]
	then
		printlog "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
	fi
	printlog "************************************************************************************"
	printlog "**                     RESUMEN                                                    **"
	printlog "************************************************************************************"
	printlog "Servidor      : `uname -n`"
	printlog "Usuario       : `whoami`" 
	printlog "Programa      : ${INF_PGM}"
	printlog "ID proceso    : ${INF_PID}"
	printlog "Hora_Inicio   : ${INF_INI}"
	printlog "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog "Parametros    : ${INF_ARG}"
	printlog "Cant. Parametr: ${INF_CANT_ARG}"
	printlog "Resultado     : ${2} "
	printlog "Archivo Log   : ${sLOG}/${ARCHLOG}"
	printlog "************************************************************************************"
	exit $1
}


###################################################################
#					Ejecuta archivo ambiente  					  #
#	    Valida la existencia del archivo de Configuracion 		  #
###################################################################

printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaAmbiente ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaAmbiente"
	echo " "
	SALIR 2 "ERROR: No se encuentra el archivo $RutaAmbiente"
fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaAmbiente
then
	echo "Archivo $RutaAmbiente OK"
	. $RutaAmbiente
else
	echo "Error, Archivo sin datos: $RutaAmbiente"
	SALIR 2 "Error, Archivo sin datos: $RutaAmbiente"
fi
printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaFunc ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaFunc"
	echo " "
	SALIR 2 "ERROR: No se encuentra el archivo $RutaFunc"
fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaFunc
then
	echo "Archivo $RutaFunc OK"
	. $RutaFunc
else
	echo "Error, Archivo sin datos: $RutaFunc"
	SALIR 2 "Error, Archivo sin datos: $RutaFunc"
fi

Eliminar_Archivo $DirLog/$SHELL_LOG
Eliminar_Archivo $DirLog/$ARCHLOG

sFecha=`date '+%d/%m/%Y'`
sHora=`date '+%X'`

#---------------------------
Realiza_Conexion ()
{

#---------------------------------------------------------------------
#  OBTIENE DATOS DE CONEXION A LA BASE DE DATOS
#---------------------------------------------------------------------

printlog " "
printlog "*********************************"
printlog "*   OBTTENE DATOS DE CONEXION   *"
printlog "*       A LA BASE DE DATOS      *"
printlog "*********************************"
echo " "
Mensaje "--> Datos de conexion a BD:"

Mensaje "Obteniendo Usuario y Password para ambiente sybase..."
printlog "Obteniendo Usuario y Password para ambiente sybase..."


for uyprsp in `UyPuser $UyPuser`
do
   uypDa[$uypcnt]=$uyprsp
      (( uypcnt=$uypcnt+1))
done
uypcnt=0
printlog "if [ ${uypDa[0]} = Falla ]"
if [ "${uypDa[0]}" = "Falla" ] ; then
   echo "No se pudo obtener la clave por ${uypDa[1]}"  >> $HOME/log/$SHELL_LOG
   echo "No se pudo obtener la clave por ${uypDa[1]}" 
   SALIR 2 "No se pudo obtener la clave por ${uypDa[1]}"
 else
 
   SYB_U=${uypDa[0]}
   SYB_P=${uypDa[1]}
   
   Mensaje "Usuario y Passwword...OK"
   printlog "Usuario y Passwword...OK"
   
fi
}

Elimina_periodo ()
{

printlog "borrando periodo..."
Mensaje "borrando periodo..."
isql -U${SYB_U} -S${DSQUERY} << eof >> $DirLog/$ARCHLOG
`echo ${SYB_P}`
use $1
go
delete from
	$2
	where
		$3 between '$4' and '$5'
go
eof
#--------------------------------------------------------------------------------
#Siempre validar que el comando se ejecuto en forma correcta
#--------------------------------------------------------------------------------
if [[ $? != 0 ]] then
   Mensaje "  ERROR : La ejecucion del borrado"
   printlog "  ERROR : La ejecucion del borrado"
   exit 2
elif [ -f $DirLog/$ARCHLOG ]; then
   verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:' $DirLog/$ARCHLOG `
   if [[ "${verE}" != "0" ]]; then
		Mensaje "  ERROR : Sybase retorno una excepcion"
	cat $DirLog/$ARCHLOG
	  exit 2
   fi
   else
	   Mensaje "  ERROR : No se Elimino el periodo"
	   cat $DirLog/$ARCHLOG
	   exit 2
   fi
printlog "...La ejecucion del borrado OK"
Mensaje "...La ejecucion del borrado OK"
}
#-------------------------

printlog "if [ $# -eq 3 ]"
FecFin=$3
FecIni=$4
if [ $# -eq 3 ]
then
	Mensaje "Cantidad de parametros correcta"
	printlog "Cantidad de parametros correcta"
	FecIni=$3
else
	if [ $# -eq 4 ]
	then
		Mensaje "Cantidad de parametros correcta"
		printlog "Cantidad de parametros correcta"
	else
		Mensaje "Cantidad de parametros incorrecta"
		printlog "Cantidad de parametros incorrecta"
		SALIR 2 "Cantidad de parametros incorrecta"
	fi
fi
	
#Comienza proceso

Realiza_Conexion $UyPuser

Elimina_periodo $bd $1 "dec_fec" $FecIni $FecFin

printlog "Tabla a cargar $1"


Arch1='hlp_historico_largo_plazo'
Arch2='hcp_historico_corto_plazo'
Arch3='hlf_historico_lea_fac'
Arch4='cru_cruce'

if [ $1 = $Arch1 ]
then
archCarga="${1}.txt"
fi

if [ $1 = $Arch2 ]
then
archCarga="${1}.txt"
fi

if [ $1 = $Arch3 ]
then
archCarga="${1}.txt"
fi

if [ $1 = $Arch4 ]
then
archCarga="${1}.txt"
fi

if [ $1 = $Arch1 -o $1 = $Arch2 -o $1 = $Arch3 -o $1 = $Arch4 ]
then
	Realiza_BcpIn $bd "$1" "$DirIn/$archCarga" "|"
else
	Realiza_BcpIn $bd "$1" "$DirOut/$1" "|"
fi


alias_fec=`expr substr $1 1 3`

hist=$2

printlog "if [ $1 = dec_detalle_cliente ]"
if [ $1 = "dec_detalle_cliente" ]
then 
	hist=2
	Mantiene_Historia $bd $1 dec $hist $3
else
	Mantiene_Historia $bd $1 dec $hist $3
fi

Mensaje ""
Mensaje "Fin shell BWS_Insercion.sh"
date '+%Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG

printlog ""
printlog "Hora de Termino "$Hora
printlog " Fin Shell BWS_Insercion.sh"
SALIR 0 "OK"
