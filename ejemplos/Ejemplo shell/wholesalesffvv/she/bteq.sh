#!/usr/bin/sh
#*********************************************************************************
# Nombre: bteq.sh
# Ruta: interfacesCustodia/she/
# Autor: Rodrigo Cordova Vivanco (ADA Ltda). - Ing. SW BCI: Carlos Lara
# Fecha: 08/11/2016
# Descripción: Shell que ejecuta modulo (Bteq) parametrico en DWH (Teradata) 
# Parámetros de Entrada: Script_sql
# Ejemplo de ejecución: sh bteq.sh Script_sql
#**********************************************************************************

fname=$1
archLog=$DirLog/${fname}_LOG.txt

Mensaje ()
{
        sFec=`date +'%d/%m %X'`
        echo "($sFec) $1"
}

###################################################################
#						funcion Printlog  						  #
###################################################################
printlog()
{
     sFec=`date +'%d/%m %X'`
        echo "($sFec) $@" >> $HOME/log/$SHELL_LOG

	
}

SALIR()
{
	printlog "*********************************************************"
	printlog "**                 RESUMEN                             **"
	printlog "*********************************************************"
	printlog "Servidor    : `uname -n`"
	printlog "Usuario     : `whoami`"
	printlog "Ambiente    : `BciAmbiente`"
	printlog "Programa    : ${INF_PGM}"
	printlog "ID proceso  : ${INF_PID}"
	printlog "Hora Inicio : ${INF_INI}"
	printlog "Hora Termino: `date +%d/%m/%Y\" \"%H:%M:%S`"
	printlog "Parametros  : ${INF_ARG}"
	printlog "Resultado   : $2"
	printlog "Archivo Log : ${LOGFILE}"
	printlog "*********************************************************"
	exit $1
}

INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@


# Paso 1: valida que el archivo exista 
if [ ! -f $DirSql/${fname}.bteq ]
then 
   echo "ERROR... El archivo ${fname}.bteq NO existe"
   printlog "ERROR... El archivo ${fname}.bteq NO existe"
   exit 2
fi

# Paso 2: elimina fila de inicio
tail +2 $DirSql/$fname.bteq > $DirSql/SCR_$fname.bteq

# Paso 3: remplaza Base de datos temporal y Vistas
cat $DirSql/SCR_$fname.bteq | sed 's/'${TabTemp1}'/'\
${TabTemp2}'/g' | sed 's/'${Vistas1}'/'${Vistas2}'/g' > $DirSql/Script_$fname.bteq

# Paso 4: Arma script
Mensaje "Arma el Script ${fname}..."
echo "bteq <<EOB" > $DirShe/${fname}_v2
echo ".RUN FILE=$LogonFile" >> $DirShe/${fname}_v2
more $DirSql/Script_$fname.bteq >> $DirShe/${fname}_v2
#echo ";" >> $DirShe/${fname}_v2 # Por si falta newline al final del script
echo ".QUIT 0;" >> $DirShe/${fname}_v2
echo "" >> $DirShe/${fname}_v2
echo "EOB" >> $DirShe/${fname}_v2

# Paso 5: ejecuta el script y luego lo borra
Mensaje "Ejecuta el Script ${fname}..."
. $DirShe/${fname}_v2 >$archLog 2>&1

# Paso 6: captura estado actual de ejecución
rc=$?

# Paso 7: borramos archivos temporales
rm $DirSql/SCR_$fname.bteq
rm $DirSql/Script_$fname.bteq
rm $DirShe/${fname}_v2

# Paso 8: Ahora esta verificando estatus del BTEQ...
Mensaje "Validando ejecucion ${fname}..."
if [ $rc -ne 0 ]
then
	Mensaje "ERROR en BTEQ - Status final: $rc"
	Mensaje ""
	SALIR 11 "ERROR en BTEQ - Status final: $rc"
else
	Mensaje "OK en BTEQ - Status final: $rc"
	Mensaje ""
	SALIR $rc "OK en BTEQ - Status final: $rc"
fi
