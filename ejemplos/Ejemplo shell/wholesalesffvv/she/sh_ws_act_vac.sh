#!/usr/bin/sh

#************************************************************************
# Nombre: sh_ws_act_vac.sh
# Ruta: $HOME/she
# Autor: Sebastian Rozas (ADA LTDA) - Ing. SW BCI: 
# Fecha: 26/12/2018 
# Descripción: Realiza procesos y extracciones en teradata
# Parámetros de Entrada:ARCHIVO
# Ejemplo de ejecucion:sh sh_ws_act_vac.sh File_sql
#************************************************************************
		

#--------------------------------------------------------------------------------
#	Carga Informacion de Ejecucion
#--------------------------------------------------------------------------------
	INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
	BSE_PGM=`echo ${INF_PGM} |cut -d"." -f1`
	INF_PID=$$
	INF_ARG=$@
	INF_CANT_ARG=$#
	INF_INI=`date +%d/%m/%y" "%H:%M:%S`
	INF_PRINTLOG="NO"
	INF_SOBRELOG="NO"

# LOG PREVIO A LA CARGA DEL ARCHIVO PROFILE
archLog=$HOME/log/${BSE_PGM}_${INF_PID}.log
#--------------------------------------------------------------------------------
#	DEFINICION DE FUNCIONES BASICAS
#--------------------------------------------------------------------------------
printlogfinal()
{	
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\""\
			"\"%H:%M:%S` #####"
		echo "" > ${archLogFnl}
		
		# Si el archivo de log final no existe pero se puedo crear con la linea anterior, se tras-
		#pasa el resultado de la ejecucion del log temporal al log final.
		echo "Valida si log final fue creado correctamente" >> ${archLogFnl}
		if test -s ${archLogFnl}
		then
			echo "" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "####### --->                  INICIO EJECUCION `date +%d/%m/%y`                "\
					"<--- ###############" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/"\
				"%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
			echo "" ${archLogFnl}
			cat ${archLog} >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###"
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###" >> ${archLogFnl}
			rm -f ${archLog}
		fi
}

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog 2 "valida retorno de salida"
	if [ $1 -gt 0 ]
	then
		printlog 2 "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
		unifica_archivos_log
	fi
	printlog 1 "************************************************************************************"
	printlog 1 "**                     RESUMEN                                                    **"
	printlog 1 "************************************************************************************"
	printlog 1 "Servidor      : `uname -n`"
	printlog 1 "Usuario       : `whoami`" 
	printlog 1 "Programa      : ${INF_PGM}"
	printlog 1 "ID proceso    : ${INF_PID}"
	printlog 1 "Hora_Inicio   : ${INF_INI}"
	printlog 1 "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog 1 "Parametros    : ${INF_ARG}"
	printlog 1 "Cant. Parametr: ${INF_CANT_ARG}"
	printlog 1 "Resultado     : ${2} "
	printlog 1 "Archivo Log   : ${archLogFnl}"
	printlog 1 "************************************************************************************"
	echo $2 | tee -a ${archLog}
	printlogfinal
	exit $1
}

printlog()
{
	# Parametro 1: Usar "1" para que la informacion salga por pantalla y quede en el log o "2" para que solo quede en el log
	# Parametro 2: El contenido a desplegar
	Hora=$(date +%H:%M:%S)
	echo "${Hora} valida printlog"   >> ${archLog}
	if [ $1 -eq 1 -o "x${INF_PRINTLOG}" = "xSI" ]
	then
		echo ${Hora} "$2" # Salida a Consola
	fi
	echo ${Hora} "$2"  >> ${archLog}
}
#----------------------------------------------------
#	Funciones previa carga del archivo de funciones.
#----------------------------------------------------

# Funcion que valida la existencia de un archivo y que tenga mas de 0 byte
func_valida_archivo ()
{
	printlog 2 "valida la existencia de un archivo"
	if [ ! -f $1 ]
	then 
		printlog 1 "Funcion func_valida_archivo : ERROR No se encuentra el archivo $1."
		SALIR 2 "FALLIDO - No se encuentra el archivo $1"
	fi

	#verifica archivo distinto de vacio
	printlog 2 "verifica archivo distinto de vacio"
	if [ -s $1 ]
	then
		printlog 2 "Funcion func_valida_archivo : Archivo $1 OK"
	else
		printlog 2 "Funcion func_valida_archivo : ERROR - El archivo $1 no posee datos."
		SALIR 2 "FALLIDO - El archivo $1 no posee datos"
	fi
}

# Funcion que valida la ejecucion o carga de un archivo (Profile y funciones)
func_valida_carga_archivo ()
{
	printlog 1 "Funcion func_valida_carga_archivo : Validando carga del $2 de funciones..."
	if [ $1 -ne 0 ]
	then
		printlog 2 "Funcion func_valida_carga_archivo : ERROR Archivo $2 no cargado."
		SALIR 2 "FALLIDO - Archivo $2 no cargado."
	else
		printlog 1 "Funcion func_valida_carga_archivo : Archivo $2 encontrado y cargado."
	fi
}

# Funcion que describe como invocar el proceso
Syntax ()
{
	printlog 1 "Sintaxis:"
	printlog 1 "\t ${INF_PGM} [-d FECHA_PROCESO] [-l LOG_FILE] [-e] [-r] ARCHIVO"
	printlog 1 ""
	printlog 1 "Opciones:"
	printlog 1 "-d\t Date, acepta la fecha de proceso con el formato YYYYMMDD"
	printlog 1 "-e\t Error, elimina las tablas en caso de existir un error durante\n\tla ejecucion del"\
			"proceso"
	printlog 1 "-l\t Log, establece el lugar donde se guardaran los log de ejecucion.\n\tSe anade la "\
		"extension .log"
	printlog 1 "-p\t Printlog, cambia la configuracion predeterminada para desplegar informacion por"\
			"pantalla"
	printlog 1 "-r\t Retorno, el retorno del proceso sera el del utilitario bteq."
	printlog 1 "-s\t Sobreescritura de Log, cambia la configuracion predeterminada para sobreescribir"\
		"o no el archivo Log"
	printlog 1 ""
	printlog 1 "Ejemplo:"
	printlog 1 "${INF_PGM} -d 20151004 -e File_sql"
	printlog 1 "${INF_PGM} -d 20151004 File_sql"
	printlog 1 "${INF_PGM} -d 20151004 -l LOG_FILE -r -s -p File_sql"
	printlog 1 "${INF_PGM} -e File_sql"
	printlog 1 "${INF_PGM} File_sql"

}

#--------------------------------------------------------------------------------
# Definicion de archivo Profile y de Funciones
#--------------------------------------------------------------------------------
ArchivoProfile=$HOME/cfg/ws_profile

# Verifica archivo Profile
func_valida_archivo $ArchivoProfile

. $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile


#verifica existencia de archivo de funciones
func_valida_archivo $ArchivoFunciones
. $ArchivoFunciones
func_valida_carga_archivo $? $ArchivoFunciones

valida_definicion_variables "${VARIABLE_AMBIENTE}"

#--------------------------------------------------------------------------------
# COMIENZA SHELL
#--------------------------------------------------------------------------------
archLogFnl=""
printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
# Cantidad minima de parametro
Cant_Min_Param=1
printlog 2 "Asignacion de Variable: Cant_Min_Param=${Cant_Min_Param}"
RetornoBteq=""
printlog 2 "Asignacion de Variable: RetornoBteq=${RetornoBteq}"
FECHA_PROCESO=$(date +%Y%m%d)
printlog 2 "Asignacion de Variable: FECHA_PROCESO=${FECHA_PROCESO}"
ANO_MES=$(expr substr ${FECHA_PROCESO} 1 6)
printlog 2 "Asignacion de Variable: ANO_MES=${ANO_MES}"

Valida_num_param $# $Cant_Min_Param

# Asignacion del Archivo de Proceso (script que sera ejecutado)
ArchivoProceso=$(echo "$@" | awk 'BEGIN {FS=" "} ; END{print $NF}' )
printlog 2 "Asignacion de Variable: ArchivoProceso=${ArchivoProceso}"

ArchivoProcesoSExt=$(echo $ArchivoProceso | cut -d'.' -f1)
printlog 2 "Asignacion de Variable: ArchivoProcesoSExt=${ArchivoProcesoSExt}"

ArchivoProcesoExte=$(echo $ArchivoProceso | cut -d'.' -f2)
printlog 2 "Asignacion de Variable: ArchivoProcesoExte=${ArchivoProcesoExte}"

# Si no se ha asignado un archivo LOG como parametro, se define uno por defecto.
printlog 2 "Asignacion del log final"
if [ "x${archLogFnl}" = "x" ]
then
	archLogFnl=${DirLog}/${BSE_PGM}.${ArchivoProcesoSExt}.${FECHA_PROCESO}.log
	printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
fi

printlog 2 "Valida si debe sobreescribir el log"
if [ "x$INF_SOBRELOG"="xSI" ]
then
	eliminar_archivo ${archLogFnl}
fi

# Paso 1: valida que el archivo btq exista 
valida_archivo ${DirFue}/$ArchivoProceso

# Paso 2: Arma script
LOGON=""
printlog 2 "Asignacion de Variable: LOGON=${LOGON}"
obtiene_medio_conexion

valida_definicion_variables "${VAR_DWH}"
construye_sql_tmp $ArchivoProceso

vac=1
while [ $vac -gt 0 ];
do 
# Paso 3: Ejecuta el script
	printlog 1 "Ejecuta el Script ${INF_PID}_$ArchivoProcesoSExt.tmp ..."
	. ${DirTmp}/${INF_PID}_$ArchivoProcesoSExt.tmp > ${DirTmp}/ejebteq.log 2>&1
	valida_ejecucion $?
	cat ${DirTmp}/ejebteq.log >> ${archLog}
#paso 3.5 verificacion de vacantes y asignacion de variable para while 
	grep -i ">>>" ${DirTmp}/ejebteq.log > ${DirTmp}/cant_vacantes
	vac=`sed -n 3p ${DirTmp}/cant_vacantes | awk 'BEGIN {FS="|"}; {gsub(/^[ \t]+/, "", $2); print $2}'`
	echo $vac
done

# Paso 4: borrado de archivos temporales
printlog 2 "Eliminando archivos temporales."
eliminar_archivo ${DirTmp}/${INF_PID}_$ArchivoProcesoSExt.tmp
eliminar_archivo ${DirTmp}/${INF_PID}_${ArchivoProcesoSExt}2.tmp
eliminar_archivo ${DirTmp}/cant_vacantes
eliminar_archivo ${DirTmp}/ejebteq.log

# Paso 5: Finaliza la ejecucion
printlog 1 "Ejecucion Correcta"
SALIR 0  "CORRECTO"
