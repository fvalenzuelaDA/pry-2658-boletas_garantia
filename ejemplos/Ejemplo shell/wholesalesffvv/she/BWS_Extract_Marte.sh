#!/usr/bin/sh
#************************************************************************
# Nombre				: BWS_Extract_Marte.sh
# Ruta					: /she/
# Autor					: Andres Riveros (Ada) - Ing. SW BCI: Alvaro Torres Medina
# Fecha					: 25/09/2017
# Descripción			: Ejecuta extraccion desde marte para Wholesale
# Parámetros de Entrada	: Año mes de proceso (AAAAMM)
# 						  Año mes de Inicio (AAAAMM)(Opcional)
# Ejemplo de ejecución	: sh BWS_Extract_Marte.sh usuario password 201704
# 						  sh BWS_Extract_Marte.sh usuario password 201709 201701
#************************************************************************
# Mantención #1: 
# Autor: Nicolas Salazar (Ada) - Ing. SW BCI: Ricardo Ramirez
# Fecha: 07/11/2018 
# Descripción: Se agrega logica para obtener bancas desde archivo parametrico
#************************************************************************"

sNOMBRE_SHELL="BWS_Extract_Marte.sh"

sLOG=$HOME/log
sCFG=$HOME/cfg
SHELL_LOG=`date +'%Y%m%d'.BWS_Extract_Marte.log`
ARCHLOG=`date +'%Y%m%d'.BWS_Extract_Marte`
date '+  %Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG
server=`uname -n`
dia=`date +'%Y'/'%m'/'%d'`
Hora=`date +'%H':'%M':'%S'`
AMB=`BciAmbiente`
RutaAmbiente="$HOME/cfg/BWS_ambiente.txt"
INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@


printlog()
{
	Hora=`date +%H:%M:%S`
	echo ${Hora} "$@" # Salida a Consola
	echo ${Hora} "$@"  >> $sLOG/${SHELL_LOG}
}

SALIR()
{
	printlog "*********************************************************"
	printlog "**                 RESUMEN                             **"
	printlog "*********************************************************"
	printlog "Servidor    : `uname -n`"
	printlog "Usuario     : `whoami`"
	printlog "Ambiente    : `BciAmbiente`"
	printlog "Programa    : ${INF_PGM}"
	printlog "ID proceso  : ${INF_PID}"
	printlog "Hora Inicio : ${INF_INI}"
	printlog "Hora Termino: `date +%d/%m/%Y\" \"%H:%M:%S`"
	printlog "Parametros  : ${INF_ARG}"
	printlog "Resultado   : $2"
	printlog "Archivo Log : ${DirLog}/${SHELL_LOG}"
	printlog "*********************************************************"
	exit $1
}

###################################################################
#					Ejecuta archivo ambiente  					  #
#	    Valida la existencia del archivo de Configuracion 		  #
###################################################################

if [ ! -f $RutaAmbiente ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaAmbiente"
	printlog "ERROR: No se encuentra el archivo $RutaAmbiente"
	echo " "
	exit 2
fi
if test -s $RutaAmbiente
then
	echo "Archivo $RutaAmbiente OK"
	printlog "Archivo $RutaAmbiente OK"
	. $RutaAmbiente
else
	echo "Error, Archivo sin datos: $RutaAmbiente"
	printlog "Error, Archivo sin datos: $RutaAmbiente"
	exit 2
fi

if [ ! -f $RutaFunc ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaFunc"
	printlog "ERROR: No se encuentra el archivo $RutaFunc"
	echo " "
	exit 2
fi
if test -s $RutaFunc
then
	echo "Archivo $RutaFunc OK"
	printlog "Archivo $RutaFunc OK"
	. $RutaFunc
else
	echo "Error, Archivo sin datos: $RutaFunc"
	printlog "Error, Archivo sin datos: $RutaFunc"
	exit 2
fi

Eliminar_Archivo $DirLog/$SHELL_LOG
Eliminar_Archivo $DirLog/$ARCHLOG

sFecha=`date '+%d/%m/%Y'`
sHora=`date '+%X'`

if [ $# -eq 1 -o $# -eq 2 ]
	then
		Mensaje "Cantidad de parametros correcta: $#"
		Mensaje ""
		printlog "Cantidad de parametros correcta: $#"
		printlog ""
	else
		Mensaje "Cantidad de parametros incorrecta: $#"
		Mensaje ""
		printlog "Cantidad de parametros incorrecta: $#"
		printlog ""
		exit 2
	fi

# Logica obtencion de banca

Valida_Archivo ${DirCfg}/ws_bancas_util.txt

export BANCAS=`awk '{ BANCAS=BANCAS sprintf("|%s|,",$1); } END{ print BANCAS }' ${DirCfg}/ws_bancas_util.txt \
| sed "s/\|/\'/g" | sed 's/.$//'`

AnoMes=$1
AnoMesIni=${AnoMes}
Ano=`echo ${AnoMes} | cut -c1-4`
IndHist=0

if [ $# -eq 2 ]
then
	AnoMesIni=$2
	Mensaje "Ejecucion Historicas desde ${AnoMesIni} hasta ${AnoMes}. Parametros: $#"
	Mensaje ""
	printlog "Ejecucion Historicas desde ${AnoMesIni} hasta ${AnoMes}. Parametros: $#"
	printlog ""
	IndHist=1
fi

#Comienza proceso

Realiza_Conexion $UyPuserMarte

if [ $IndHist -eq 0 ]
then
	printlog "Indicador de ejecucion Historica: $IndHist"
	
	MesCoa=`echo ${AnoMes} | cut -c5,6`
	Ejecuta_Consulta_BWS_Extract_Data
	Ejecuta_Consulta_BWS_Extract_Coa ${MesCoa}

	leer_lista ${DirCfg}/BWS_configuracion.txt ${DirLog}/${ARCHLOG} $DirIn
fi

if [ $IndHist -eq 1 ]
then
	printlog "Indicador de ejecucion Historica: $IndHist"
	Ejecuta_Consulta_BWS_Extract_Data
	Ejecuta_Consulta_BWS_Extract_Coa_Hist

	leer_lista ${DirCfg}/BWS_configuracion.txt ${DirLog}/${ARCHLOG} $DirIn
fi

SALIR 0 "Ejecucion Exitosa"