#!/usr/bin/sh
#*********************************************************************************
# Nombre: fload.sh
# Ruta: interfacesCustodia/she/
# Autor: Rodrigo Cordova Vivanco (ADA Ltda). - Ing. SW BCI: Carlos Lara
# Fecha: 08/11/2016
# Descripción: Shell que ejecuta modulo (FastLoad) parametrico en DWH (Teradata) 
# Parámetros de Entrada: Script_sql
# Ejemplo de ejecución: sh fload.sh Script_sql
#**********************************************************************************

fname=$1
archLog=$DirLog/${fname}_LOG.txt

Mensaje ()
{
        sFec=`date +'%d/%m %X'`
        echo "($sFec) $1"
}

###################################################################
#						funcion Printlog  						  #
###################################################################
printlog()
{
     sFec=`date +'%d/%m %X'`
        echo "($sFec) $@" >> $HOME/log/$SHELL_LOG

	
}

SALIR()
{
	printlog "*********************************************************"
	printlog "**                 RESUMEN                             **"
	printlog "*********************************************************"
	printlog "Servidor    : `uname -n`"
	printlog "Usuario     : `whoami`"
	printlog "Ambiente    : `BciAmbiente`"
	printlog "Programa    : ${INF_PGM}"
	printlog "ID proceso  : ${INF_PID}"
	printlog "Hora Inicio : ${INF_INI}"
	printlog "Hora Termino: `date +%d/%m/%Y\" \"%H:%M:%S`"
	printlog "Parametros  : ${INF_ARG}"
	printlog "Resultado   : $2"
	printlog "Archivo Log : ${LOGFILE}"
	printlog "*********************************************************"
	exit $1
}

INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@


# Paso 1: valida que el archivo exista 
if [ ! -f $DirSql/${fname}.fld ]
then 
   echo "ERROR... El archivo ${fname}.fld NO existe"
   printlog "ERROR... El archivo ${fname}.fld NO existe"
   exit 2
fi

DS_Conn=`more $LogonFile` 	#Contenido del archivo de conexion

# Paso 2: elimina fila de inicio
tail +2 $DirSql/$fname.fld > $DirSql/SCR_$fname.fld

# Paso 3: remplaza Base de datos temporal y Vistas
cat $DirSql/SCR_$fname.fld | sed 's/'${TabTemp1}'/'\
${TabTemp2}'/g' | sed 's/'${Vistas1}'/'${Vistas2}'/g' > $DirSql/Script_$fname.fld


# Paso 4: Arma script
Mensaje "Arma el Script ${fname}..."
echo "fastload <<EOB" > $DirShe/${fname}_v2
echo "${DS_Conn}" >> $DirShe/${fname}_v2
more $DirSql/Script_$fname.fld >> $DirShe/${fname}_v2
#echo ";" >> $DirShe/${fname}_v2 # Por si falta newline al final del script
echo ".QUIT 0;" >> $DirShe/${fname}_v2
echo "" >> $DirShe/${fname}_v2
echo "EOB" >> $DirShe/${fname}_v2

# Paso 5: ejecuta el script y luego lo borra
Mensaje "Ejecuta el Script ${fname}..."
. $DirShe/${fname}_v2 >$archLog 2>&1

# Paso 6: captura estado actual de ejecución
rc=$?

# Paso 7: borramos archivos temporales
rm $DirSql/SCR_$fname.fld
rm $DirSql/Script_$fname.fld
rm $DirShe/${fname}_v2

# Paso 8: Ahora esta verificando estatus del FASTLOAD...
Mensaje "Validando ejecucion ${fname}..."
if [ $rc -ne 0 ]
then
	Mensaje "ERROR en FASTLOAD - Status final: $rc"
	Mensaje ""	
	SALIR 11 "ERROR en FASTLOAD - Status final: $rc"
else
	Mensaje "OK en FASTLOAD - Status final: $rc"
	Mensaje ""
	SALIR $rc "OK en FASTLOAD - Status final: $rc"
fi