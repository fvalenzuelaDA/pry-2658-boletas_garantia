#!/usr/bin/sh
#************************************************************************
# Nombre				: sh_ws_ins_syb.sh.sh
# Ruta					: /she/
# Autor					: Sebastian Rozas (Ada) - Ing. SW BCI: Alvaro Torres Medina
# Fecha					: 02/03/2018
# Descripción			: carga tablas
# Parámetros de Entrada	: <tabla>
#						  <ruta/nombre archivo sybase>
#						  <ruta/nombre archivo dwh>
# Ejemplo de ejecución	: sh sh_ws_ins_syb.sh <tabla> <ruta/archivo_sybase> <ruta/archivo2_dwh>
#************************************************************************

sNOMBRE_SHELL="sh_ws_ins_syb.sh.sh"

sLOG=$HOME/log
sCFG=$HOME/cfg

printlog()
{
	Hora=`date +%H:%M:%S`
	echo ${Hora} "$@" # Salida a Consola
	echo ${Hora} "$@"  >> $sLOG/${SHELL_LOG}
}

SHELL_LOG=`date +'%Y%m%d'.sh_ws_ins_syb.sh.${1}.log`
ARCHLOG=`date +'%Y%m%d'.sh_ws_ins_syb.sh.${1}`
date '+  %Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG
server=`uname -n`
dia=`date +'%Y'/'%m'/'%d'`
Hora=`date +'%H':'%M':'%S'`
AMB=`BciAmbiente`
RutaAmbiente="$HOME/cfg/BWS_ambiente.txt"
INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog "if [ $1 -gt 0 ]"
	if [ $1 -gt 0 ]
	then
		printlog "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
	fi
	printlog "************************************************************************************"
	printlog "**                     RESUMEN                                                    **"
	printlog "************************************************************************************"
	printlog "Servidor      : `uname -n`"
	printlog "Usuario       : `whoami`" 
	printlog "Programa      : ${INF_PGM}"
	printlog "ID proceso    : ${INF_PID}"
	printlog "Hora_Inicio   : ${INF_INI}"
	printlog "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog "Parametros    : ${INF_ARG}"
	printlog "Cant. Parametr: ${INF_CANT_ARG}"
	printlog "Resultado     : ${2} "
	printlog "Archivo Log   : ${sLOG}/${ARCHLOG}"
	printlog "************************************************************************************"
	exit $1
}


###################################################################
#					Ejecuta archivo ambiente  					  #
#	    Valida la existencia del archivo de Configuracion 		  #
###################################################################

printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaAmbiente ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaAmbiente"
	echo " "
	SALIR 2 "ERROR: No se encuentra el archivo $RutaAmbiente"
fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaAmbiente
then
	echo "Archivo $RutaAmbiente OK"
	. $RutaAmbiente
else
	echo "Error, Archivo sin datos: $RutaAmbiente"
	SALIR 2 "Error, Archivo sin datos: $RutaAmbiente"
fi
printlog "Valida existencia de archivo: $RutaAmbiente "
if [ ! -f $RutaFunc ]
then 
	echo " "
	echo "ERROR: No se encuentra el archivo $RutaFunc"
	echo " "
	SALIR 2 "ERROR: No se encuentra el archivo $RutaFunc"
fi
printlog "Valida contenido de archivo: $RutaAmbiente"
if test -s $RutaFunc
then
	echo "Archivo $RutaFunc OK"
	. $RutaFunc
else
	echo "Error, Archivo sin datos: $RutaFunc"
	SALIR 2 "Error, Archivo sin datos: $RutaFunc"
fi

Eliminar_Archivo $DirLog/$SHELL_LOG
Eliminar_Archivo $DirLog/$ARCHLOG

sFecha=`date '+%d/%m/%Y'`
sHora=`date '+%X'`

#---------------------------
Realiza_Conexion ()
{

#---------------------------------------------------------------------
#  OBTIENE DATOS DE CONEXION A LA BASE DE DATOS
#---------------------------------------------------------------------

printlog " "
printlog "*********************************"
printlog "*   OBTTENE DATOS DE CONEXION   *"
printlog "*       A LA BASE DE DATOS      *"
printlog "*********************************"
echo " "
Mensaje "--> Datos de conexion a BD:"

Mensaje "Obteniendo Usuario y Password para ambiente sybase..."
printlog "Obteniendo Usuario y Password para ambiente sybase..."


for uyprsp in `UyPuser $UyPuser`
do
   uypDa[$uypcnt]=$uyprsp
      (( uypcnt=$uypcnt+1))
done
uypcnt=0
printlog "if [ ${uypDa[0]} = ERROR ]"
if [ "${uypDa[0]}" = "ERROR" ] ; then
   echo "No se pudo obtener la clave por ${uypDa[1]}"  >> $HOME/log/$SHELL_LOG
   echo "No se pudo obtener la clave por ${uypDa[1]}" 
   SALIR 2 "No se pudo obtener la clave por ${uypDa[1]}"
 else
 
   SYB_U=${uypDa[0]}
   SYB_P=${uypDa[1]}
   
   Mensaje "Usuario y Password...OK"
   printlog "Usuario y Password...OK"
   
fi
}

Realiza_BcpIn ()
{ # Paratmeros: $1 = BD    $2 = nombre_tabla   $3 = Ruta/nombre_archivo $4 = separador $5=DSQUERY

printlog "Carga archivo $3 en tabla $2"
Mensaje "Carga archivo $3 en tabla $2"

if [ -s $3 ]
    then
	numreg=`wc -l $3| awk '{ print $1 }'`
	Mensaje "Registros a Cargar en tabla $2: $numreg"
	printlog "Registros a Cargar en tabla $2: $numreg"  

	
	bcp $1..$2 in $3 -U${SYB_U} -P${SYB_P} -S$5 -c -A8192 -b1000  -Jiso_1 -t $4 -e $DirLog/shell_bcp.err >> $DirLog/$ARCHLOG

	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $? != 0 ]] then 
		if [ -f $DirLog/$ARCHLOG ]; then
			Eper=`egrep -c 'with unique index' $DirLog/$ARCHLOG`
			if [[ "${Eper}" != "0" ]]; then
				Mensaje "ERROR : periodo cargado ya existe en BD"
				printlog "ERROR : periodo cargado ya existe en BD"
				exit 2
			fi
		fi
	   Mensaje "ERROR : El BCP IN a la tabla $2 se cancelo"
	   printlog "ERROR : El BCP IN a la tabla $2 se cancelo"
	   exit 2
	fi
	if [ -f $DirLog/$ARCHLOG ]; then
		Eper=`egrep -c 'with unique index' $DirLog/$ARCHLOG`
		if [[ "${Eper}" != "0" ]]; then
			Mensaje "ERROR : periodo cargado ya existe en BD"
			printlog "ERROR : periodo cargado ya existe en BD"
			exit 2
		fi
	fi
	if [ -f $DirLog/$ARCHLOG ]; then
	   verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirLog/$ARCHLOG`
	   if [[ "${verE}" != "0" ]]; then
	      Mensaje "ERROR : BCP IN a la tabla $2 retorno una excepcion"
	      printlog "ERROR : BCP IN a la tabla $2 retorno una excepcion"
	      exit 2
	   fi
	fi
	Mensaje "...carga de archivo $3 OK"
	printlog "...carga de archivo $3 OK"
	Mensaje ""
	printlog ""	
	
else
	Mensaje "Archivo $3 no se genero con datos"
	printlog "Archivo $3 no se genero con datos"
	exit 0
fi
}

Ejecuta_Truncate ()
{ #paremtero: $1 = BD  $2 = Nombre_Tabla $3=DSQUERY

printlog "Borrando registros tabla $2..."
Mensaje "  Borrando registros tabla $2..."

	
Mensaje "Ejecuta Truncate..."
printlog "Ejecuta Truncate..."
isql -U${SYB_U} -S${3} << eof >> $DirLog/$ARCHLOG
`echo ${SYB_P}`
use $1 
go
truncate table $2
go
eof

		#--------------------------------------------------------------------------------
		# Siempre validar que el comando se ejecuto en forma correcta
		#--------------------------------------------------------------------------------
		if [[ $? != 0 ]] then
		   Mensaje "ERROR : La ejecucion del Truncate a la tabla $2 se cancelo"
		   printlog "ERROR : La ejecucion del  Truncate a la tabla $2 se cancelo"
		   exit 2
		elif [ -f $DirLog/$ARCHLOG ]; then
		   verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:' $DirLog/$ARCHLOG `
		   if [[ "${verE}" != "0" ]]; then
		        Mensaje "ERROR : Sybase retorno una excepcion al ejecutar Truncata a la tabla $2"
				printlog "ERROR : Sybase retorno una excepcion al ejecutar Truncata a la tabla $2"
		      exit 2
		   fi
		   else
		       Mensaje "ERROR : La ejecucion del Truncate a la tabla $2 se cancelo"
			   printlog "ERROR : La ejecucion del Truncate a la tabla $2 se cancelo"
		       exit 2
		 fi

Mensaje "...Ejecucion del Truncate $2 Ok"
printlog "...Ejecucion del Truncate $2 Ok"
echo " "
}
#-------------------------

printlog "Validando la cantidad de parametros"
if [ $# -eq 3 -o $# -eq 4 ]
	then
		Mensaje "Cantidad de parametros correcta"
		printlog "Cantidad de parametros correcta"
	else
		Mensaje "Cantidad de parametros incorrecta"
		printlog "Cantidad de parametros incorrecta"
		SALIR 2 "Cantidad de parametros incorrecta"
	fi
	
#Comienza proceso

Realiza_Conexion $UyPuser

printlog "Validando existencia del archivo del 2do parametro"
if [ ! -f $3 ]
then
	Mensaje "El archivo del 2do parametro no existe"
	printlog "El archivo del 2do parametro no existe"
	SALIR 2 "El archivo del 2do parametro no existe"
fi

printlog "Validando si es necesario concatenar archivos"
if [ $# -eq 4 ]
then
	printlog "Es preciso concatenar archivos"
	printlog "Validando existencia del archivo del 3er parametro"
	if [ ! -f $4 ]
	then
		Mensaje "El archivo del 3er parametro no existe"
		printlog "El archivo del 3er parametro no existe"
		SALIR 2 "El archivo del 3er parametro no existe"
	fi
	cat $4 >> $3
	if [ $? -ne 0 ]
	then
		Mensaje "Error al concatenar los archivos"
		printlog "Error al concatenar los archivos"
		SALIR 2 "Error al concatenar los archivos"
	fi
fi

Ejecuta_Truncate $bd $1 $2
Realiza_BcpIn $bd "$1" "$3" ";" "$2"

Mensaje ""
Mensaje "Fin shell sh_ws_ins_syb.sh.sh"
date '+%Y-%m-%d %H:%M:%S' >> $sLOG/$ARCHLOG

printlog ""
printlog "Hora de Termino "$Hora
printlog " Fin Shell sh_ws_ins_syb.sh.sh"
SALIR 0 "OK"
