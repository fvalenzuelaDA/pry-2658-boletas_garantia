

SELECT DATE, TIME;
DIAGNOSTIC "COLLECTSTATS, SAMPLESIZE=10" ON FOR SESSION;
/* **************************************************************************************************** */
/* ************************************ HISTORICO BOLETA GARANTIA ************************************* */
/* **************************************************************************************************** */
/* CREACION				:	16 MARZO DEL 2020															*/
/* MODULO				:	EXTRACTOR DE HISTORIRIA DE BOLETA DE GARANTIAS								*/
/* DESCRIPCION			:	SE EXTRAE INFORMACION HISTORICA DE BOLETAS DE GARANTIA, PARA LUEGO SER 		*/
/*							APLANADA Y EXPORTADA AL MODULO DE CONSULTA DE FINANCIAMIENTO (postgresql)	*/
/* TABLA DE ENTRADA		:	##MKT_EXPLORER_VAR##.VENTA_DIARIA_COL										*/
/* TABLA DE SALIDA		:	##BD_TEMP##.BGT_HIST_COL_INS													*/
/* EMPRESA				:	ADA LTDA.																	*/
/* **************************************************************************************************** */


/* **************************************************************************************************** */
/* TABLA QUE VA A CONTENER TODOS LOS PARTY_ID DEL UNIVERSO DE CLIENTES									*/
/* **************************************************************************************************** */
DROP TABLE ##BD_TMP##.BGT_HIST_COL;
CREATE TABLE ##BD_TMP##.BGT_HIST_COL
(
 OPERACION			VARCHAR(20)
,RUT				INTEGER
,DV					VARCHAR(1)
,TIPO_AUX			VARCHAR(10)
,TIPO				VARCHAR(10)
,MONEDA				VARCHAR(10)
,MTO_CREDITO		DECIMAL(18,4)
,FECHA_CUR_RE		TIMESTAMP(0)
,ESTADO				VARCHAR(10)
,QUIEN_ACTIVA		VARCHAR(25)
,FECHA_PROX_VCTO	TIMESTAMP(0)
,CTA_ABONO			VARCHAR(50)
,DESTINO_CRED		VARCHAR(150)
,CANAL_CURSE		VARCHAR(100)
)PRIMARY INDEX ( OPERACION );

.IF ERRORCODE <> 0 THEN .QUIT 1;


--INSERTA REGISTROS DE BLG
INSERT INTO ##BD_TMP##.BGT_HIST_COL
SELECT
	COALESCE(OPERACION,'')		
	,ZEROIFNULL(RUT)			
	,COALESCE(DV,'')							
	,TRIM(TIPO||AUX)   AS TIPO_AUX			
	,COALESCE(TIPO,'')			
	,COALESCE(MONEDA,'')			
	,ZEROIFNULL(MTO_CREDITO)	
	,CASE WHEN FECHA_CUR_RE IS NULL THEN '1900-01-01 00:00:00' ELSE (TRIM(FECHA_CUR_RE) ||  ' 00:00:00') END 
	,COALESCE(ESTADO,'')		
	,COALESCE(QUIEN_ACTIVA,'')	
	,CASE WHEN FECHA_PROX_VCTO IS NULL THEN '1900-01-01 00:00:00' ELSE (TRIM(FECHA_PROX_VCTO) ||  ' 00:00:00') END
	,COALESCE(CTA_ABONO,'')		
	,COALESCE(DESTINO_CRED,'')
	,COALESCE(CANAL_CURSE,'')
FROM
	##MKT_EXPLORER_VAR##.VENTA_DIARIA_COL
WHERE
	ESTADO = 'A'
AND FECHA_CARGA BETWEEN ('##FECHA_PROCESO1##'(DATE,FORMAT 'YYYYMMDD'))  AND ('##FECHA_PROCESO2##'(DATE,FORMAT 'YYYYMMDD'))
;
		
.IF ERRORCODE <> 0 THEN .QUIT 2;

COLLECT STATISTICS USING SAMPLE ##BD_TMP##.BGT_HIST_COL COLUMN OPERACION;
.IF ERRORCODE <> 0 THEN .QUIT 3;
COLLECT STATISTICS USING SAMPLE ##BD_TMP##.BGT_HIST_COL COLUMN RUT;
.IF ERRORCODE <> 0 THEN .QUIT 4;
COLLECT STATISTICS USING SAMPLE ##BD_TMP##.BGT_HIST_COL COLUMN DV;
.IF ERRORCODE <> 0 THEN .QUIT 5;



/* **************************************************************************************************** */
/* TABLA QUE CONTIENE SENTENCIAS DE INSERT SOLICITADOS POR EL CLIENTE
/* LOS CUALES SERAN APLANADOS EN UN ARCHIVO															*/
/* **************************************************************************************************** */
DROP TABLE ##BD_TMP##.BGT_HIST_COL_INS;
CREATE TABLE ##BD_TMP##.BGT_HIST_COL_INS
(
 CAMPO_INSER VARCHAR(1000) --781
) UNIQUE PRIMARY INDEX(CAMPO_INSER)
;

IF ERRORCODE <> 0 THEN .QUIT 6;


--SE INSERTA REGISTROS CON INSERT SOLICITADOS POR EL CLIENTE
INSERT INTO ##BD_TMP##.BGT_HIST_COL_INS 
select 'insert into public.bgt_solicitud (bgt_num_ope,bgt_emp_rut,bgt_emp_vrt,bgt_emp_nom,bgt_mto_sol,bgt_mon_sol,bgt_fec_ven,bgt_mto_com,bgt_mto_com_prc'
||',bgt_cta_car,bgt_ben_rut,bgt_ben_vrt,bgt_ben_nom,bgt_ben_num_con,bgt_ben_email,bgt_ben_tip,bgt_ben_tip_pag,bgt_ben_obj'
||',bgt_ben_glosa,bgt_estado,bgt_fec_gen,bgt_usu_asign,bgt_tipo,bgt_tipo_linea,bgt_canal) values ('
|| '''' ||TRIM(OPERACION) ||''''|| ',' || TRIM(RUT) || ',' ||''''|| TRIM(DV) ||''''|| ',' || '''''' || ',' || TRIM(MTO_CREDITO) || ',' ||''''|| TRIM(MONEDA) ||''''|| ',' ||''''||cast(FECHA_PROX_VCTO as varchar(19)) 
||''''|| ',' || TRIM(0) || ',' || TRIM(0) || ',' ||''''|| TRIM(CTA_ABONO) ||''''|| ',' || TRIM(0) || ',' || '''''' || ',' || '''''' || ',' || '''''' || ',' || '''''' || ',' ||''''||  TRIM(TIPO) ||''''|| ',' 
|| '''''' || ',' ||''''|| TRIM(DESTINO_CRED) ||''''|| ',' || '''''' || ',' ||''''|| TRIM(ESTADO) ||''''|| ',' ||''''|| cast(FECHA_CUR_RE as varchar(19)) ||''''|| ',' ||''''|| TRIM(QUIEN_ACTIVA) ||''''|| ',' ||''''|| TRIM(TIPO) ||''''|| ',' || '''''' || ',' ||''''|| TRIM(CANAL_CURSE)||''''|| ');' || 'go'
from
	##BD_TMP##.BGT_HIST_COL
;
.IF ERRORCODE <> 0 THEN .QUIT 7;

COLLECT STATISTICS USING SAMPLE ##BD_TMP##.BGT_HIST_COL_INS COLUMN CAMPO_INSER;
.IF ERRORCODE <> 0 THEN .QUIT 8;

DROP TABLE ##BD_TMP##.BGT_HIST_COL;

SELECT DATE, TIME;
.LOGOFF;
.QUIT 0;
