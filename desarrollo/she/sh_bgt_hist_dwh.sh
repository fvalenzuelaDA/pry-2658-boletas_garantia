#!/usr/bin/sh

#************************************************************************
# Nombre: sh_bgt_hist_dwh.sh
# Ruta: $HOME/she
# Autor: ADA LTDA
# Fecha: 03/2020
# Descripción: Realiza procesos y extracciones en teradata
# Parámetros de Entrada: [-d FECHA_PROCESO1 FECHA_PROCESO2]  [-l LOG_FILE] [-e] [-r] ARCHIVO
#				-d:Date, acepta la fecha de proceso con el formato YYYYMMDD
#				-e: Error, elimina las tablas en caso de existir un error durante la ejecucion
#				-l: Log, establece el lugar donde se guardaran los log de ejecucion.
#				-p: Printlog, cambia la configuracion para desplegar informacion por pantalla
#				-r: Retorno, el retorno del proceso sera el del utilitario bteq.
#				-s: Sobreescritura de Log, cambia la configuracion para sobreescribir o no en Log
# Ejemplo de ejecución: sh sh_blg_hist_dwh.sh cgd_cliente_genero_diario.sql 
#				sh sh_bgt_hist_dwh.sh -d 20151004 20151005 -e File_sql
#				sh sh_bgt_hist_dwh.sh -d 20151004 20151005 File_sql
#				sh sh_bgt_hist_dwh.sh -d 20151004 20151005 -l LOG_FILE -r -s -p File_sql
#				sh sh_bgt_hist_dwh.sh -e File_sql
#				sh sh_bgt_hist_dwh.sh File_sql
#************************************************************************
		

#--------------------------------------------------------------------------------
#	Carga Informacion de Ejecucion
#--------------------------------------------------------------------------------
	INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
	BSE_PGM=`echo ${INF_PGM} |cut -d"." -f1`
	INF_PID=$$
	INF_ARG=$@
	INF_CANT_ARG=$#
	INF_INI=`date +%d/%m/%y" "%H:%M:%S`
	INF_PRINTLOG="NO"
	INF_SOBRELOG="NO"

# LOG PREVIO A LA CARGA DEL ARCHIVO PROFILE
archLog=$HOME/tmp/${BSE_PGM}_${INF_PID}.log
export archLog
 
#--------------------------------------------------------------------------------
#	DEFINICION DE FUNCIONES BASICAS
#--------------------------------------------------------------------------------
printlogfinal()
{	
	printlog 2 "valida si log existe"
	if [ "x${archLogFnl}" = "x" ]
	then
		#Si el nombre del log real no se ha construido debido a que no se obtuvo el parametro de
		#fecha para la extension
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			 "correctos."
		echo "Favor de ver log de paso ${archLog}"
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			"correctos." >> ${archLog}
		echo "Favor de ver log de paso ${archLog}" >> ${archLog}
		return
	fi
	
	printlog 2 "valida si log tiene datos"
	if [ -s ${archLogFnl} ]
	then
		# Si existe el log final, quiere decir que ya existe una ejecucion del proceso para
		# la misma fecha, por lo cual, se debe concatenar el resultado de esta ejecucion
		# con el resultado de la ejecucion anterior en el mismo archivo.
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\""\
			"\"%H:%M:%S` #####" 
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "######### --->                  INICIO EJECUCION `date +%d/%m/%y`                  "\
			"<--- #############" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\""\
			"\"%H:%M:%S` #####" >> ${archLogFnl}
		echo "" ${archLogFnl}
		cat ${archLog} >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "##### Fin de la ejecucion  `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"\
			>> ${archLogFnl}
		rm -f ${archLog}
	else
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\""\
			"\"%H:%M:%S` #####"
		echo "" > ${archLogFnl}
		
		# Si el archivo de log final no existe pero se puedo crear con la linea anterior, se tras-
		#pasa el resultado de la ejecucion del log temporal al log final.
		echo "Valida si log final fue creado correctamente" >> ${archLogFnl}
		if test -s ${archLogFnl}
		then
			echo "" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "####### --->                  INICIO EJECUCION `date +%d/%m/%y`                "\
					"<--- ###############" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/"\
				"%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
			echo "" ${archLogFnl}
			cat ${archLog} >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###"
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###" >> ${archLogFnl}
			rm -f ${archLog}
		fi
	fi
}

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog 2 "valida retorno de salida"
	if [ $1 -gt 0 ]
	then
		printlog 2 "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
		unifica_archivos_log
	fi
	printlog 1 "************************************************************************************"
	printlog 1 "**                     RESUMEN                                                    **"
	printlog 1 "************************************************************************************"
	printlog 1 "Servidor      : `uname -n`"
	printlog 1 "Usuario       : `whoami`" 
	printlog 1 "Programa      : ${INF_PGM}"
	printlog 1 "ID proceso    : ${INF_PID}"
	printlog 1 "Hora_Inicio   : ${INF_INI}"
	printlog 1 "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog 1 "Parametros    : ${INF_ARG}"
	printlog 1 "Cant. Parametr: ${INF_CANT_ARG}"
	printlog 1 "Resultado     : ${2} "
	printlog 1 "Archivo Log   : ${archLogFnl}"
	printlog 1 "************************************************************************************"
	echo $2 | tee -a ${archLog}
	printlogfinal
	exit $1
}

printlog()
{
	# Parametro 1: Usar "1" para que la informacion salga por pantalla y quede en el log o "2" para que solo quede en el log
	# Parametro 2: El contenido a desplegar
	Hora=$(date +%H:%M:%S)
	echo "${Hora} valida printlog"   >> ${archLog}
	if [ $1 -eq 1 -o "x${INF_PRINTLOG}" = "xSI" ]
	then
		echo ${Hora} "$2" # Salida a Consola
	fi
	echo ${Hora} "$2"  >> ${archLog}
}
#----------------------------------------------------
#	Funciones previa carga del archivo de funciones.
#----------------------------------------------------

# Funcion que valida la existencia de un archivo y que tenga mas de 0 byte
func_valida_archivo ()
{
	printlog 2 "valida la existencia de un archivo"
	if [ ! -f $1 ]
	then 
		printlog 1 "Funcion func_valida_archivo : ERROR No se encuentra el archivo $1"
		SALIR 2 "FALLIDO - No se encuentra el archivo $1"
	fi

	#verifica archivo distinto de vacio
	printlog 2 "verifica archivo distinto de vacio"
	if [ -s $1 ]
	then
		printlog 2 "Funcion func_valida_archivo : Archivo $1 OK"
	else
		printlog 2 "Funcion func_valida_archivo : ERROR - El archivo $1 no posee datos."
		SALIR 2 "FALLIDO - El archivo $1 no posee datos"
	fi
}

# Funcion que valida la ejecucion o carga de un archivo (Profile y funciones)
func_valida_carga_archivo ()
{
	printlog 1 "Funcion func_valida_carga_archivo : Validando carga del $2 de funciones..."
	if [ $1 -ne 0 ]
	then
		printlog 2 "Funcion func_valida_carga_archivo : ERROR Archivo $2 no cargado."
		SALIR 2 "FALLIDO - Archivo $2 no cargado."
	else
		printlog 1 "Funcion func_valida_carga_archivo : Archivo $2 encontrado y cargado."
	fi
}

# Funcion que describe como invocar el proceso
Syntax ()
{
	printlog 1 "Sintaxis:"
	printlog 1 "\t ${INF_PGM} [-d FECHA_PROCESO1 FECHA_PROCESO2] [-l LOG_FILE] [-e] [-r] ARCHIVO"
	printlog 1 ""
	printlog 1 "Opciones:"
	printlog 1 "-d\t Date, acepta la fecha de proceso con el formato YYYYMMDD"
	printlog 1 "-e\t Error, elimina las tablas en caso de existir un error durante\n\tla ejecucion del"\
			"proceso"
	printlog 1 "-l\t Log, establece el lugar donde se guardaran los log de ejecucion.\n\tSe anade la "\
		"extension .log"
	printlog 1 "-p\t Printlog, cambia la configuracion predeterminada para desplegar informacion por"\
			"pantalla"
	printlog 1 "-r\t Retorno, el retorno del proceso sera el del utilitario bteq."
	printlog 1 "-s\t Sobreescritura de Log, cambia la configuracion predeterminada para sobreescribir"\
		"o no el archivo Log"
	printlog 1 ""
	printlog 1 "Ejemplo:"
	printlog 1 "${INF_PGM} -d 20151004 20151005 -e File_sql"
	printlog 1 "${INF_PGM} -d 20151004 20151005 File_sql"
	printlog 1 "${INF_PGM} -d 20151004 20151005 -l LOG_FILE -r -s -p File_sql"
	printlog 1 "${INF_PGM} -e File_sql"
	printlog 1 "${INF_PGM} File_sql"

}

#--------------------------------------------------------------------------------
# Definicion de archivo Profile y de Funciones
#--------------------------------------------------------------------------------
ArchivoProfile=$HOME/cfg/bgt_profile

# Verifica archivo Profile
func_valida_archivo $ArchivoProfile

. $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile


#verifica existencia de archivo de funciones
func_valida_archivo $ArchivoFunciones
. $ArchivoFunciones
func_valida_carga_archivo $? $ArchivoFunciones

valida_definicion_variables "${VARIABLE_AMBIENTE}"

#--------------------------------------------------------------------------------
# COMIENZA SHELL
#--------------------------------------------------------------------------------
archLogFnl=""
printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
# Cantidad minima de parametro
Cant_Min_Param=1
printlog 2 "Asignacion de Variable: Cant_Min_Param=${Cant_Min_Param}"
RetornoBteq=""
printlog 2 "Asignacion de Variable: RetornoBteq=${RetornoBteq}"
FECHA_PROCESO=$(date +%Y%m%d)
printlog 2 "Asignacion de Variable: FECHA_PROCESO=${FECHA_PROCESO}"
ANO_MES=$(expr substr ${FECHA_PROCESO} 1 6)
printlog 2 "Asignacion de Variable: ANO_MES=${ANO_MES}"

# Asignacion de parametros dinamicos
# d para fecha
# e para eliminar tablas en caso de error
# r para retornar el valor entregado por bteg. Con la salvedad de que el valor de retorno debe
#	caber en un byte.
# l Asigna el log, y no considera el que es por defecto.
# s Invierte la configuracion de la sobreescritura del log
# p Invierte la configuracion acerca de la informacion que se despliega por pantalla

while getopts ":d::l::e:r:p:s" opt; do
	printlog 2 "El parametro a validar es $opt."
	case $opt in
	d)
		Cant_Min_Param=$(($Cant_Min_Param+3))
		printlog 2 "Asignacion de Variable: Cant_Min_Param=${Cant_Min_Param}"
		validaPeriodo $2
		validaPeriodo $3		
		FECHA_PROCESO1=$2
		FECHA_PROCESO2=$3
		printlog 2 "Asignacion de Variable: FECHA_PROCESO1=${FECHA_PROCESO1}"
		printlog 2 "Asignacion de Variable: FECHA_PROCESO2=${FECHA_PROCESO2}"
		ANO_MES=`expr substr ${FECHA_PROCESO1} 1 6`
		
		;;
	e)
		Cant_Min_Param=$(($Cant_Min_Param+1))
		printlog 2 "Asignacion de Variable: Cant_Min_Param=${Cant_Min_Param}"
		ErrorEliminaTablas="SI"
		printlog 2 "Asignacion de Variable: ErrorEliminaTablas=${ErrorEliminaTablas}"
		;;
	l)
		Cant_Min_Param=$(($Cant_Min_Param+2))
		printlog 2 "Asignacion de Variable: Cant_Min_Param=${Cant_Min_Param}"
		archLogFnl=${DirLog}/$OPTARG
		printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
		;;
	r)
		Cant_Min_Param=$(($Cant_Min_Param+1))
		printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
		RetornoBteq="SI"
		printlog 2 "Asignacion de Variable: RetornoBteq=${RetornoBteq}"
		printlog 2 "Advertencia: Si el valor de retorno de bteq supera el byte, este sera adaptado:"\
					"Por ejemplo 666 -> 2*256+154. El retorno sera 154"
		;;
	p)
		Cant_Min_Param=$(($Cant_Min_Param+1))
		printlog 2 "Asignacion de Variable: INF_PRINTLOG=${INF_PRINTLOG}"
		if [ "x$INF_SOBRELOG"="SI" ]
		then
			INF_PRINTLOG="NO"
		else
			INF_PRINTLOG="SI"
		fi
		printlog 2 "Asignacion de Variable: INF_PRINTLOG=${INF_PRINTLOG}"
		;;
	s)
		Cant_Min_Param=$(($Cant_Min_Param+1))
		printlog 2 "Asignacion de Variable: INF_SOBRELOG=${INF_SOBRELOG}"
		if [ "x$INF_SOBRELOG"="SI" ]
		then
			INF_SOBRELOG="NO"
		else
			INF_SOBRELOG="SI"
		fi
		printlog 2 "Asignacion de Variable: INF_SOBRELOG=${INF_SOBRELOG}"
		;;
	\?)
		printlog 1 "Opcion no valida: -$OPTARG" >&2
		Syntax
		SALIR 2 "FALLIDO - La sintaxis de ejecucion no es correcta"
		;;
	:)
		printlog 2 "Opcion -$OPTARG requiere un parametro." >&2
		Syntax
		SALIR 2 "FALLIDO - La sintaxis de ejecucion no es correcta"
		;;
	esac
done

Valida_num_param $# $Cant_Min_Param


# Asignacion del Archivo de Proceso (script que sera ejecutado)
ArchivoProceso=$(echo "$@" | awk 'BEGIN {FS=" "} ; END{print $NF}' )
printlog 2 "Asignacion de Variable: ArchivoProceso=${ArchivoProceso}"

ArchivoProcesoSExt=$(echo $ArchivoProceso | cut -d'.' -f1)
printlog 2 "Asignacion de Variable: ArchivoProcesoSExt=${ArchivoProcesoSExt}"

ArchivoProcesoExte=$(echo $ArchivoProceso | cut -d'.' -f2)
printlog 2 "Asignacion de Variable: ArchivoProcesoExte=${ArchivoProcesoExte}"

# Si no se ha asignado un archivo LOG como parametro, se define uno por defecto.
printlog 2 "Asignacion del log final"
if [ "x${archLogFnl}" = "x" ]
then
	archLogFnl=${DirLog}/${BSE_PGM}.${ArchivoProcesoSExt}.${FECHA_PROCESO}.log
	printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
fi

printlog 2 "Valida si debe sobreescribir el log"
if [ "x$INF_SOBRELOG"="xSI" ]
then
	eliminar_archivo ${archLogFnl}
fi


# Paso 1: valida que el archivo btq y de entrada existan 
valida_archivo ${DirFue}/$ArchivoProceso



# Paso 2: Arma script
LOGON=""
printlog 2 "Asignacion de Variable: LOGON=${LOGON}"
obtiene_medio_conexion

valida_definicion_variables "${VAR_DWH}"
construye_sql_tmp $ArchivoProceso

# Paso 3: Ejecuta el script
printlog 1 "Ejecuta el Script ${INF_PID}_$ArchivoProcesoSExt.tmp ..."
. ${DirTmp}/${INF_PID}_$ArchivoProcesoSExt.tmp >>${archLog} 2>&1
valida_ejecucion $?

# Paso 3.5: Disponibiliza Archivo
printlog 2 "Valida si es necesario realizar la limpieza de archivo"
if [ "x"$(retorna_util $ArchivoProcesoExte) = "xfexp" ]
then
	disponibiliza_archivo $ArchivoProcesoSExt $ArchivoProcesoSExt
fi

# Paso 4: borrado de archivos temporales
printlog 2 "Eliminando archivos temporales."
eliminar_archivo ${DirTmp}/${INF_PID}_$ArchivoProcesoSExt.tmp
eliminar_archivo ${DirTmp}/${INF_PID}_${ArchivoProcesoSExt}2.tmp


# Paso 5: Finaliza la ejecucion
printlog 1 "Ejecucion Correcta"
SALIR 0  "CORRECTO"
