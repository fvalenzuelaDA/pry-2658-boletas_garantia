#!/usr/bin/sh
#*********************************************************************************
# Nombre: fexp.sh
# Ruta: interfacesCustodia/she/
# Autor: Rodrigo Cordova Vivanco (ADA Ltda). - Ing. SW BCI: Carlos Lara
# Fecha: 08/11/2016
# Descripci�n: Shell que ejecuta modulo (FastExport) parametrico en DWH (Teradata) 
# Par�metros de Entrada: Script_sql
# Ejemplo de ejecuci�n: sh fexp.sh Scrip_sql
#**********************************************************************************

fname=$1
archLog=$DirLog/${fname}_LOG.txt
INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
INF_PID=$$
INF_INI=`date +%d/%m/%Y" "%H:%M:%S`
INF_ARG=$@

Mensaje ()
{
        sFec=`date +'%d/%m %X'`
        echo "($sFec) $1"
}

###################################################################
#						funcion Printlog  						  #
###################################################################
printlog()
{
     sFec=`date +'%d/%m %X'`
        echo "($sFec) $@" >> $HOME/log/$SHELL_LOG

	
}

#funcion salir
SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog "if [ $1 -gt 0 ]"
	if [ $1 -gt 0 ]
	then
		printlog "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
	fi
	printlog "************************************************************************************"
	printlog "**                     RESUMEN                                                    **"
	printlog "************************************************************************************"
	printlog "Servidor      : `uname -n`"
	printlog "Usuario       : `whoami`" 
	printlog "Programa      : ${INF_PGM}"
	printlog "ID proceso    : ${INF_PID}"
	printlog "Hora_Inicio   : ${INF_INI}"
	printlog "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog "Parametros    : ${INF_ARG}"
	printlog "Cant. Parametr: ${INF_CANT_ARG}"
	printlog "Resultado     : ${2} "
	printlog "Archivo Log   : ${archLog}"
	printlog "************************************************************************************"
	exit $1
}

# Paso 1: valida que el archivo exista 
if [ ! -f $DirSql/${fname}.fexp ]
then 
   echo "ERROR... El archivo ${fname}.fexp NO existe"
   printlog "ERROR... El archivo ${fname}.fexp NO existe"
   SALIR 2 "ERROR... El archivo ${fname}.fexp NO existe"
fi

DS_Conn=`more $LogonFile` 	#Contenido del archivo de conexion

# Paso 2: elimina fila de inicio
tail +2 $DirSql/$fname.fexp > $DirSql/SCR_$fname.fexp

# Paso 3: remplaza Base de datos temporal y Vistas
cat $DirSql/SCR_$fname.fexp | sed 's/'${TabTemp1}'/'\
${TabTemp2}'/g' | sed 's/'${Vistas1}'/'${Vistas2}'/g' > $DirSql/Script_$fname.fexp

#cat $DirSql/SCR_$fname.fexp | sed 's/'${NomArch1}'/'${NomArch2}'/g' >> $DirSql/Script_$fname.fexp

# Paso 4: Arma script
Mensaje "Arma el Script ${fname}..."
echo "fexp <<EOB" > $DirShe/${fname}_v2
echo "${DS_Conn}" >> $DirShe/${fname}_v2
more $DirSql/Script_$fname.fexp >> $DirShe/${fname}_v2
#echo ";" >> $DirShe/${fname}_v2 # Por si falta newline al final del script
echo ".QUIT 0;" >> $DirShe/${fname}_v2
echo "" >> $DirShe/${fname}_v2
echo "EOB" >> $DirShe/${fname}_v2

# Paso 5: ejecuta el script y luego lo borra
Mensaje "Ejecuta el Script ${fname}..."
. $DirShe/${fname}_v2 >$archLog 2>&1

# Paso 6: captura estado actual de ejecuci�n
rc=$?

# Paso 7: borramos archivos temporales
rm $DirSql/SCR_$fname.fexp
rm $DirSql/Script_$fname.fexp
rm $DirShe/${fname}_v2

# Paso 8: Ahora esta verificando estatus del FASTEXPORT...
Mensaje "Validando ejecucion ${fname}..."
if [ $rc -ne 0 ]
then
	Mensaje "ERROR en FASTEXPORT - Status final: $rc"
	Mensaje ""
	printlog "ERROR en FASTEXPORT - Status final: $rc"
	SALIR 2 "ERROR en FASTEXPORT - Status final: $rc"
else
	Mensaje "OK en FASTEXPORT - Status final: $rc"
	Mensaje ""
	printlog "OK en FASTEXPORT - Status final: $rc"
fi

# Paso 9: termina ejecuci�n
#Mensaje "${rc}"
SALIR $rc "OK"
